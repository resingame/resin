﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AgeDrawer2 : LPDrawParticleSystem
{
    public LPParticleSystem sys;
    public float scale = 1f;
    public LPParticleMaterial zombie;
    List<int> delindices = new List<int>();

    public override void UpdateParticles(LPParticle[] partdata)
    {
        delindices = new List<int>();
        if (GetComponent<ParticleEmitter>().particleCount < partdata.Length)
        {
            GetComponent<ParticleEmitter>().Emit(partdata.Length - GetComponent<ParticleEmitter>().particleCount);
            particles = GetComponent<ParticleEmitter>().particles;
        }

        for (int i = 0; i < particles.Length; i++)
        {
            if (Random.value < scale)
            {
                //particles[i] = null;
            }
            
        }

        if (delindices.Count > 0)
        {
            delindices.Insert(0, delindices.Count);
            LPAPIParticles.SetSelectedParticleFlags(sys.GetPtr(), delindices.ToArray(), zombie.GetInt());
        }

        GetComponent<ParticleEmitter>().particles = particles;
    }
}
