﻿using System;
using KingDOM.Event;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using KingDOM.Event.Debug;

[Serializable]
public class EventsLog
{

    public List<EventLogLine> events = new List<EventLogLine>();
    private List<EventLogLine> openEvents = new List<EventLogLine>();

    public void Init()
    {
        events = new List<EventLogLine>();
        openEvents = new List<EventLogLine>();
        Sender.startEvent += Add;
        Sender.endEvent += Add;
        Sender.stopEvent += Add;
        Sender.errorEvent += Add;

    }

    public void Destroy()
    {
        Sender.startEvent -= Add;
        Sender.endEvent -= Add;
        Sender.stopEvent -= Add;
        Sender.errorEvent -= Add;
        events = new List<EventLogLine>();
    }
    public void Add(object target, EventCheckArgs args)
    {
        EventLogLine currentEvent = events.Find(x => x.Event == args.Event);
        if (currentEvent == null)
        {
            if ((args.Stage & EventsCheckStage.Before) > 0)
            {
                EventLogLine newEvent = new EventLogLine(args);
                //openEvents.Add(newEvent);
                events.Add(newEvent);
            }

        }
        else
        {
            currentEvent.Update(args);
            //openEvents.Remove(currentEvent);

        }

    }
    public void Clear()
    {
        events = new List<EventLogLine>();
    }
}
