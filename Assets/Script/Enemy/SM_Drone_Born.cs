﻿using UnityEngine;
using System.Collections;

public class SM_Drone_Born : StateMachineBehaviour
{
    public DroneController drone;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        if (drone != null)
        {
            drone.GetDirection();
        }
    }
}
