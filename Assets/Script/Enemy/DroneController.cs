﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using KingDOM;
using KingDOM.Event;

public class DroneController : MonoBehaviour
{
    #region public_var
    [AnimatorParameter(AnimatorParameterAttribute.ParameterType.Float)]
    public int ParmNameMove;

    [AnimatorParameter(AnimatorParameterAttribute.ParameterType.Trigger)]
    public int ParmNameShot;

    [AnimatorParameter(AnimatorParameterAttribute.ParameterType.Float)]
    public int ParmNameHealth;

    [AnimatorParameter(AnimatorParameterAttribute.ParameterType.Bool)]
    public int ParmNameDetect;

    [AnimatorParameter(AnimatorParameterAttribute.ParameterType.Bool)]
    public int ParmNameWaiting;

    [AnimatorParameter(AnimatorParameterAttribute.ParameterType.Bool)]
    public int ParmNameCanShot;

    [AnimatorState]
    public int StateBorn;
    [AnimatorState]
    public int StateMove;
    [AnimatorState]
    public int StateShot;
    [AnimatorState]
    public int StateDie;
    [AnimatorState]
    public int StateAlert;

    public Collider2D trigger = null;
    public Vector2 direction = Vector2.zero;
    public int NumShots = 1;
    public float SpeedLimit = 4;
    public float Accelerate = 1;
    public float waitTime = 1;
    public float SpeedDetect = 1;
    public float ScanTime = 1;
    public float shotDistance = 1.5f;
    public LayerMask maskCollision;
    public SM_Weapon weapon;
    #endregion

    #region private_var
    private bool isOk = false;
    private Rigidbody2D body;
    private Animator animator;
    private Dictionary<Behaviour, float> targets;
    private List<Behaviour> targetsCurrent;
    private Vector3 mainTarget;
    private Vector3 defaultPoint;
    private float sqrSpeedDetect = 0f;
    private Transform TargetSelected = null;
    private int currentState;
    #endregion

    #region unity_methods

    // Use this for initialization
    void Awake()
    {
        animator = GetComponent<Animator>();
        isOk = true;
        isOk = KingUtil.CheckNotNull(animator, "Не удалось найти ссылку на аниматор", isOk, this);
        isOk = KingUtil.CheckNotNull(ParmNameMove, "Не удалось найти ссылку на параметр состояния движения", isOk, this);
        isOk = KingUtil.CheckNotNull(ParmNameHealth, "Не удалось найти ссылку на параметр состояния живой", isOk, this);
        isOk = KingUtil.CheckNotNull(ParmNameShot, "Не удалось найти ссылку на параметр состояния выстрел", isOk, this);
        isOk = KingUtil.CheckNotNull(ParmNameDetect, "Не удалось найти ссылку на параметр поиска цели", isOk, this);
        isOk = KingUtil.CheckNotNull(ParmNameWaiting, "Не удалось найти ссылку на параметр ожидания", isOk, this);
        isOk = KingUtil.CheckNotNull(ParmNameCanShot, "Не удалось найти ссылку на параметр можно стрелять", isOk, this);
        Initialize();
        isOk = KingUtil.CheckNotNull(trigger, "Не удалось обнаружить триггер обнаружения противника.", isOk, this);
        isOk = KingUtil.CheckNotNull(body, "Не удалось обнаружить тело физики.", isOk, this);
        isOk = KingUtil.CheckNotNull(weapon, "Не удалось обнаружить оружие дрона.", isOk, this);


    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!isOk) return;
        GetCurrentState();
        if (currentState == StateDie)
        {
            Destroy(gameObject, 1f);

        }
        else if (currentState == StateShot && TargetSelected != null)
        {
            TargetSelected = null;
            weapon.StartShot = true;
        }
        animator.SetBool(ParmNameCanShot, weapon.CanShoot);
        MoveToTarget();
        ScanTarget();
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (!isOk) return;
        Rigidbody2D rb = other.attachedRigidbody;
        if (!TargetSelected && rb.velocity.sqrMagnitude > sqrSpeedDetect)
        {
            Behaviour behaviour = rb.GetComponentInParent<Behaviour>();
            if (behaviour != null && !targetsCurrent.Contains(behaviour))
            {
                targetsCurrent.Add(behaviour);
            }

        }

    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (!isOk) return;
        SelectDefaultTarget();
        mainTarget = defaultPoint;
    }

    void OnCollisionStay2D(Collision2D coll)
    {
        if (!isOk) return;
        SelectDefaultTarget();
        mainTarget = defaultPoint;
    }

    private void OnDestroy()
    {
        Sender.RemoveEvent(EventName.WARRIOR_READY, hnWarriorReady);
    }

    #endregion

    #region private_methods
    /// <summary>
    /// Получить текущее состояние из аниматора
    /// </summary>
    void GetCurrentState()
    {
        AnimatorStateInfo state = animator.GetCurrentAnimatorStateInfo(0);
        currentState = state.fullPathHash;
    }

    /// <summary>
    /// Получить направление движения после столкновени
    /// </summary>
    public void GetDirection()
    {
        Vector2 dir = Vector2.zero;
        for (int i = 0; i < 10; i++)
        {
            dir = new Vector2(Random.value - 0.5f, Random.value - 0.5f); dir.Normalize();
            if (!Physics2D.Raycast(transform.position, dir, WeaponBase.TileSize / 2, maskCollision))
            {
                direction = dir;
                return;
            }
        }
    }

    /// <summary>
    /// Сброс состояния после наступления события готов следующий воин
    /// </summary>
    /// <param name="evnt">событие</param>
    private void hnWarriorReady(SimpleEvent evnt)
    {
        ResetState();
    }

    /// <summary>
    /// Инициализация начального состояния
    /// </summary>
    void Initialize()
    {
        if (!isOk) return;

        trigger = GetComponent<Collider2D>();
        targets = new Dictionary<Behaviour, float>();
        targetsCurrent = new List<Behaviour>();
        body = GetComponent<Rigidbody2D>();
        sqrSpeedDetect = SpeedDetect * SpeedDetect;
        SelectDefaultTarget();
        mainTarget = defaultPoint;
        if (weapon == null) weapon = transform.parent.GetComponentInChildren<SM_Weapon>();

        Sender.AddEvent(EventName.WARRIOR_READY, hnWarriorReady);
    }

    /// <summary>
    /// Двжиение дрона к цели
    /// </summary>
    void MoveToTarget()
    {
        Vector3 dir = (mainTarget - transform.position).normalized;
        Vector3 speed = body.velocity;
        animator.SetFloat(ParmNameMove, speed.x);
        Vector3 acc = dir * Accelerate;
        // тормозим если кто-то двигается или дистанция до основной цели уже меньше зоны выстрела
        if (!TargetSelected && targetsCurrent.Count != 0 || Vector3.Distance(transform.position, mainTarget) <= shotDistance)
        {
            acc *= -1;
            if (acc.sqrMagnitude > speed.sqrMagnitude) acc = speed * -1;
        }
        speed += acc * Time.fixedDeltaTime;
        speed = Vector2.ClampMagnitude(speed, SpeedLimit);
        Vector3 newPosition = transform.position + speed * Time.fixedDeltaTime;

        body.MovePosition(transform.position + speed * Time.fixedDeltaTime);
        weapon.Position = body.transform.position;
    }

    /// <summary>
    /// Сброс в начальное состояние
    /// </summary>
    private void ResetState()
    {
        TargetSelected = null;
        weapon.Reload();
    }

    /// <summary>
    /// Поиск цели
    /// </summary>
    void ScanTarget()
    {
        Transform selectTarget = null;
        if (!TargetSelected)
        {
            foreach (var behaviour in targetsCurrent)
            {
                if (targets.ContainsKey(behaviour))
                {
                    targets[behaviour] += Time.fixedDeltaTime;
                }
                else
                {
                    targets.Add(behaviour, Time.fixedDeltaTime);
                }
            }
        }
        if (TargetSelected == null && targets.Count > 0)
        {
            float maxVal = targets.Values.Max();
            var pair = targets.FirstOrDefault(k => k.Value == maxVal);
            selectTarget = pair.Key.transform;
            if (weapon.CanShoot && pair.Value > ScanTime)
            {
                animator.SetTrigger(ParmNameShot);
                weapon.StartShot = true;
                TargetSelected = pair.Key.transform;
                targets.Remove(pair.Key);
            }
        }
        mainTarget = TargetSelected != null && weapon.CanShoot ? TargetSelected.position : defaultPoint;
        // Устанавливаем прицеливание если у нас есть выбранная цель
        if (TargetSelected != null) selectTarget = TargetSelected;
        if (selectTarget != null)
        {
            weapon.gameObject.SetActive(true);
            GishState gish= selectTarget.GetComponentInChildren<GishState>();
            weapon.NewDirection = gish == null? selectTarget.position: gish.Center - transform.position;
        }
        targetsCurrent = new List<Behaviour>();
    }

    /// <summary>
    /// Выбрать цель по умолчанию, точки в пространстве для патрулирования.
    /// </summary>
    void SelectDefaultTarget()
    {
        GetDirection();
        Vector2 displ = direction * 1000 * WeaponBase.TileSize;
        defaultPoint = transform.position + new Vector3(displ.x, displ.y);
    }

    #endregion
}
