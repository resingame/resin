﻿using UnityEngine;
using System.Collections;

public class SM_Destroy : StateMachineBehaviour
{
    [Range(0,10)]
    public float Timer = 1f;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        Destroy(animator.gameObject, Timer);
    }
}
