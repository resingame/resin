﻿using UnityEngine;
using System.Collections;
using System;

public class ThrowObject : MonoBehaviour {

    [ComponentSelect]
    public SM_Weapon weapon;
    public float ForceThrow = 10f;

    public GameObject ObjectForThrow = null;
    public bool useLastDirection = false;

    public void OnEnable()
    {
        if (weapon.LastDirection.sqrMagnitude < weapon.DeadZone * weapon.DeadZone) return;

        if (ObjectForThrow == null) return;

        //DestroyAim();
        GameObject bomb = Instantiate(ObjectForThrow);
        WeaponUtility.Rotate(bomb, weapon.Position, WeaponUtility.Direction2Angle(weapon.LastDirection));
        Rigidbody2D rig = bomb.GetComponentInChildren<Rigidbody2D>();
        rig.AddForce(weapon.LastDirection.normalized * ForceThrow * (useLastDirection ? 1: weapon.NewDirection.magnitude), ForceMode2D.Impulse);
        Debug.Log("ForceThrow: " + (ForceThrow * weapon.NewDirection.magnitude).ToString());
        weapon.gameObject.SetActive(false);
    }

}
