﻿using UnityEngine;
using System.Collections;
using KingDOM.Event;

public class Laser : WeaponBase
{
    public void  Start()
    {
         element = Reaction.Element.Light;
        if (owner != null)
        {
            GishControllerNew controller = owner.GetComponentInChildren<GishControllerNew>();
            if (controller != null) Aim(controller.Center, LastDirection);
        }
        
    }

    public override void Aim(Vector2 position, Vector2 direction)
    {
        if (aimCurrent == null)
        {
            aimCurrent = CreateAndRotate(AimVisual, position, direction);
        }
        else
        {
            Rotate(aimCurrent, position, direction);
        }

    }

    public override void Shot(Vector2 position, Vector2 direction)
    {
        if (direction.SqrMagnitude() < float.Epsilon) return;

        //DestroyAim();
        CreateAndRotate(ShotVisual,position, direction);
        Behaviour myBehaivour = owner.GetComponentInParent<Behaviour>();
        RaycastHit2D[] hits = Physics2D.RaycastAll(position, direction, distance * TileSize, mask.value);
        foreach (RaycastHit2D hit in hits)
        {
            if (hit.collider != null)
            {
                Behaviour behaivour = hit.transform.GetComponentInParent<Behaviour>();
                if (behaivour != myBehaivour) behaivour.Effect(element, strength);
            }
        }
        DestroyAim();
        //Sender.SendEvent(EventName.NEXT_TEAM, this);
        base.Shot(position, direction);
    }
}

