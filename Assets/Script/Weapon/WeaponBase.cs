﻿using System;
using UnityEngine;
using System.Collections;
using KingDOM.Event;

[Serializable]
public class WeaponBase : MonoBehaviour
{

    public Reaction.Element element = Reaction.Element.Light;
    public float strength = 30;
    public float distance = 3;
    public float shotPower = 1f;
    public float speedPower = 1f;
    public int ShotsNumber = 1;
    public float TimeAfterLastShot = 5f;
    public LayerMask mask;
    public GameObject AimVisual = null;
    public GameObject ShotVisual = null;
    public float DeadZone = 0.3f;
    public GameObject owner = null;
    private Vector2 _lastDirection = Vector2.up;

    public static float TileSize = 2f;

    protected GameObject aimCurrent = null;
    protected int leftShot = 0;

    void OnDisable()
    {
        DestroyAim();
    }

    public bool CanShoot
    {
        get { return leftShot > 0; }
    }

    public virtual Vector2 LastDirection
    {
        get { return _lastDirection; }
        set
        {
            Vector2 direction = Vector2.zero;
            if (Mathf.Abs(value.x) > DeadZone)
            {
                direction = direction + Vector2.right * value.x;
            }
            else if (Mathf.Abs(value.y) > DeadZone)
            {
                direction = Vector2.up * value.y;
            }
            _lastDirection = direction;
        }
    }

    public void Reload()
    {
        leftShot = ShotsNumber;
    }

    virtual public void Aim(Vector2 position, Vector2 direction)
    {

    }
    virtual public void Aim(Vector2 position)
    {
        Aim(position, _lastDirection);
    }

    virtual public void DestroyAim()
    {
        if (aimCurrent != null)
        {
            Destroy(aimCurrent);
            aimCurrent = null;
        }
    }

    virtual public void Shot(Vector2 position, Vector2 direction)
    {

        leftShot--;
        if (leftShot <= 0)
            Sender.SendEvent(EventName.TIME_START, this, ParmName.TIME, TimeAfterLastShot);
    }
    virtual public void Shot(Vector2 position)
    {

        Shot(position, _lastDirection);
    }

    virtual public void ShotPowerReset()
    {
        
    }

    virtual public bool ShotPower()
    {
        return true;
    }

    protected void Rotate(GameObject obj, Vector2 position, Vector2 direction)
    {
        if (obj != null)
        {
            float angle = Direction2Angle(direction);
            obj.transform.position = position;
            obj.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }

    protected GameObject CreateAndRotate(GameObject obj, Vector2 position, Vector2 direction)
    {
        if (obj != null)
        {
            float angle = Direction2Angle(direction);
            GameObject go = Instantiate(obj, position, Quaternion.AngleAxis(angle, Vector3.forward)) as GameObject;
            go.transform.parent = transform;
            return go;
        }
        return null;
    }
    protected GameObject CreateAndRotate(GameObject obj, Vector2 position)
    {
        return CreateAndRotate(obj, position, _lastDirection);
    }

    protected float Direction2Angle(Vector2 direction)
    {
        float angle = 0f;
        if (direction.x > DeadZone)
        {
            angle = 0;
        }
        else if (direction.x < -DeadZone)
        {
            angle = 180;
        }
        else if (direction.y > DeadZone)
        {
            angle = 90;
        }
        else if (direction.y < -DeadZone)
        {
            angle = 270;
        }
        else
        {
            if (Mathf.Abs(_lastDirection.sqrMagnitude - direction.sqrMagnitude) > float.Epsilon)
            {
                angle = Direction2Angle(_lastDirection);
            }
        }
        return angle;
    }
}
