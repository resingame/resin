﻿using UnityEngine;
using System.Collections;

public class CollisionActivator : MonoBehaviour
{

    [ComponentSelect]
    public MonoBehaviour Target = null;
    public bool SetValue = false;
    public LayerMask maskCollision;
    public LayerMask MaskOwner;
    private bool lefOwner = false;
    private Collider2D myCollider = null;

    void Awake()
    {
        lefOwner = false;
        myCollider = GetComponent<Collider2D>();
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (CanExplode(coll.gameObject))
        {
            if (Target != null)
            {
                Target.enabled = SetValue;
                this.enabled = false;
            }
        }

    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (((1 << collision.gameObject.layer) & MaskOwner) != 0)
        {
            if (!lefOwner)
            {
                Behaviour behaviour = collision.gameObject.GetComponentInParent<Behaviour>();
                if (behaviour == SM_Weapon.OWNER && !myCollider.IsTouchingLayers(MaskOwner)) lefOwner = true; 
            }
            
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        OnTriggerEnter2D(other);
    }

    bool CanExplode(GameObject obj)
    {
        if (((1 << obj.layer) & maskCollision) != 0)
        {
            if (lefOwner) return true;
            Behaviour behaviour = obj.GetComponentInParent<Behaviour>();
            if (behaviour != SM_Weapon.OWNER) return true;
        }
        return false;
    }
}
