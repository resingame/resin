﻿using UnityEngine;
using System.Collections;

public class ShotAxisDistance : MonoBehaviour, IShot {

    public GameObject target = null;
    private float deadZone;
    // Use this for initialization
    void Awake()
    {
        if (target == null) target = gameObject;
    }

    public void SetDeadZone(float deadZone)
    {
        this.deadZone = deadZone;
    }

    public void SetActive(bool active)
    {
        target.SetActive(active);
    }

    public void Shot(Vector2 position, Vector2 direction, bool isDown, bool isPress, bool isUp)
    {
        if (direction.SqrMagnitude() < float.Epsilon) return;

        if (!isPress) return;
        
        WeaponUtility.Rotate(target, position, WeaponUtility.Direction2AngleAxis(direction, deadZone));
        //DestroyAim();
        //CreateAndRotate(ShotVisual, position, direction);
        /*Behaviour myBehaivour = owner.GetComponentInParent<Behaviour>();
        RaycastHit2D[] hits = Physics2D.RaycastAll(position, direction, distance * TileSize, mask.value);
        foreach (RaycastHit2D hit in hits)
        {
            if (hit.collider != null)
            {
                Behaviour behaivour = hit.transform.GetComponentInParent<Behaviour>();
                if (behaivour != myBehaivour) behaivour.Effect(element, strength);
            }
        }
        DestroyAim();
        //Sender.SendEvent(EventName.NEXT_TEAM, this);
        base.Shot(position, direction);*/
    }
}
