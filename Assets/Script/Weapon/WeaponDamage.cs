﻿using UnityEngine;
using System.Collections;

public class WeaponDamage : MonoBehaviour {

    private Behaviour owner = null;
    private SM_Weapon weapon = null;
    public Reaction.Element element = Reaction.Element.Fire;
    public float strength = 30f;
    public float Distance = 3f;
    public bool OnlyFirstCollider = true;
    // Use this for initialization
    void Awake()
    {
        owner = GetComponentInParent<Behaviour>();
        weapon = GetComponentInParent<SM_Weapon>();
    }

    // Update is called once per frame
    void OnEnable()
    {
        if (weapon == null) return;
        RaycastHit2D[] hits = Physics2D.RaycastAll(weapon.Position, weapon.LastDirection, Distance * WeaponBase.TileSize, weapon.mask);
        
        foreach (RaycastHit2D hit in hits)
        {
            if (hit.collider != null)
            {
                Behaviour behaivour = hit.transform.GetComponentInParent<Behaviour>();
                if (behaivour != owner)
                {
                    behaivour.Effect(element, strength);
                    if (OnlyFirstCollider) break;
                }
            }
        }
    }
}
