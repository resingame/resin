﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class Inventory 
{

    public SortedList<string, InventoryItem> items;
    private InventoryItem current = null;
    private int idxCurrent = -1;

    public InventoryItem Current
    {
        get
        {
            if (current == null && items.Count > 0)
            {
                idxCurrent = -1;
                if (current != null)
                {
                    items.TryGetValue(current.weapon.name, out current);
                }
                if (current == null)
                {
                    current = GetNext();
                }
                
            }
            return current;
        }
        set { current = value; }
    }

    public InventoryItem GetNext()
    {
        NextIdx();
        int lastValue = idxCurrent;
        do
        {
            if (idxCurrent >= 0 && idxCurrent < items.Count)
            {
                InventoryItem item = items[items.Keys[idxCurrent]];
                if (item != null && (item.unlimited || item.quantity > 0))
                {
                    Current = item;
                    return item;
                }
            }
            NextIdx();
        } while (idxCurrent != lastValue);
        return null;
    }

    private void NextIdx()
    {
        idxCurrent++;

        if (idxCurrent >= items.Count) idxCurrent = 0;
    }
    public InventoryItem GetPrevious()
    {
        PreviousIdx();
        int lastValue = idxCurrent;
        do
        {
            if (idxCurrent >= 0 && idxCurrent < items.Count)
            {
                InventoryItem item = items[items.Keys[idxCurrent]];
                if (item != null && (item.unlimited || item.quantity > 0))
                {
                    Current = item;
                    return item;
                }
            }
            PreviousIdx();
        } while (idxCurrent != lastValue);
        return null;
    }

    private void PreviousIdx()
    {
        idxCurrent--;

        if (idxCurrent < 0) idxCurrent = items.Count - 1;
    }
    // Use this for initialization
    public void Init () {
	    if (items == null) items = new SortedList<string, InventoryItem>();
	}
	
}
