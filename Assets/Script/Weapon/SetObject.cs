﻿using UnityEngine;
using System.Collections;

public class SetObject : MonoBehaviour {

    [ComponentSelect]
    public SM_Weapon weapon;
    public float Distance = 0.2f;

    public GameObject ObjectForSet = null;
    public void OnEnable()
    {
        if (weapon.LastDirection.sqrMagnitude < weapon.DeadZone * weapon.DeadZone) return;

        if (ObjectForSet == null) return;

        //DestroyAim();
        GameObject bomb = Instantiate(ObjectForSet);
        float angle = WeaponUtility.Direction2AngleHorizontal(weapon.LastDirection, weapon.DeadZone);
        Vector2 direction = angle == 0 ? Vector2.right : (angle == 180 ? Vector2.left : Vector2.zero);
        WeaponUtility.Rotate(bomb, weapon.Position + direction * Distance*SM_Weapon.TileSize, angle);
        weapon.gameObject.SetActive(false);
    }
}
