﻿using UnityEngine;
using System.Collections;
using System;

public class AimAxis : MonoBehaviour, IAim {

    public GameObject target = null;
    private float deadZone;
    private Vector2 lastDirection;
    // Use this for initialization
    void Awake () {
        if (target == null) target = gameObject;
        if (lastDirection.sqrMagnitude < float.Epsilon) lastDirection = Vector2.right;
	}
	
    public void SetDeadZone(float deadZone)
    {
        this.deadZone = deadZone;
    }
    public void Aim(Vector2 position, Vector2 direction)
    {

        WeaponUtility.Rotate(target, position, WeaponUtility.Direction2AngleAxis(direction, deadZone));
    }

    public void SetActive(bool active)
    {
        target.SetActive(active);
    }
}
