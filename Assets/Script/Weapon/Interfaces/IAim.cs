﻿using UnityEngine;
using System.Collections;

public interface IAim  {

    void SetDeadZone(float deadZone);
    void Aim(Vector2 position, Vector2 direction);
    void SetActive(bool active);
}
