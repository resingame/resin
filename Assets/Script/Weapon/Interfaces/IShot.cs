﻿using UnityEngine;
using System.Collections;

public interface IShot {

    void SetDeadZone(float deadZone);
    void SetActive(bool active);
    void Shot(Vector2 position, Vector2 direction, bool isDown, bool isPress, bool isUp);
}
