﻿using UnityEngine;
using System.Collections;

public interface IBullet
{
    GameObject Owner { set; }
}
