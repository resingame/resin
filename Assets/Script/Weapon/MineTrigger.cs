﻿using UnityEngine;
using System.Collections;
using KingDOM;

public class MineTrigger : MonoBehaviour
{

    public LayerMask mask;

    void Update()
    {
        
    }

	// Update is called once per frame
	void OnTriggerEnter2D(Collider2D other)
    {
        if (!enabled) return;
        if (((1 << other.gameObject.layer) & mask) != 0)
        {
            DestroyByTime destroyer = GetComponent<DestroyByTime>();
            if (destroyer != null) destroyer.enabled = true;
            enabled = false; 
        }
    }
    void OnTriggerStay2D(Collider2D other)
    {
        OnTriggerEnter2D(other);
    }
}
