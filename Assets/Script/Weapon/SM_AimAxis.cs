﻿using UnityEngine;
using System.Collections;

public class SM_AimAxis : StateMachineBehaviour
{
    private SM_Weapon weapon;

    private SM_Weapon GetWeapon(Animator animator)
    {
        if (weapon == null) weapon = animator.GetComponent<SM_Weapon>();
        return weapon;
    }

// OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	    GetWeapon(animator).AimObject.SetActive(true);
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	    if (GetWeapon(animator) != null)
	    {
            WeaponUtility.Rotate(weapon.AimObject, weapon.Position, WeaponUtility.Direction2AngleAxis(weapon.LastDirection, weapon.DeadZone));
	    }
	}

    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GetWeapon(animator).AimObject.SetActive(false);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
