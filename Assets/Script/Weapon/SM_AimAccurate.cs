﻿using UnityEngine;
using System.Collections;
using KingDOM;

public class SM_AimAccurate : StateMachineBehaviour
{
    public float MaxDistance = 1.5f;
    public bool UseMaxShot = false;
    private SM_Weapon weapon;

    private SM_Weapon GetWeapon(Animator animator)
    {
        if (weapon == null) weapon = animator.GetComponent<SM_Weapon>();
        return weapon;
    }

// OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	    GetWeapon(animator).AimObject.SetActive(true);
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	    if (GetWeapon(animator) != null)
	    {
	        Vector2 dir = weapon.NewDirection;
            if (UseMaxShot) dir.Normalize();
            //float angle = WeaponUtility.Direction2Angle(weapon.NewDirection);
            weapon.AimObject.SetActive(dir.sqrMagnitude > (weapon.DeadZone * weapon.DeadZone));
            //WeaponUtility.Rotate(weapon.AimObject, weapon.Position,  angle);
	        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            weapon.AimObject.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            weapon.AimObject.transform.position = weapon.Position + dir * MaxDistance * SM_Weapon.TileSize;
            //Quaternion q = Quaternion.LookRotation(weapon.NewDirection);
            //weapon.AimObject.transform.LookAt(weapon.Position + weapon.NewDirection); //= q;

        }
    }

    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GetWeapon(animator).AimObject.SetActive(false);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
