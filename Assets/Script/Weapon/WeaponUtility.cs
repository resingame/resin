﻿using UnityEngine;
using System.Collections;

public class WeaponUtility {

    public static void Rotate(GameObject obj, Vector2 position, float angle)
    {
        if (obj != null)
        {
            obj.transform.position = position;
            obj.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }

    public static float Direction2Angle(Vector2 direction)
    {
        float angle = Vector2.Angle(Vector2.right, direction);
        return angle;
    }
    public static float Direction2AngleAxis(Vector2 direction, float DeadZone)
    {
        float angle = -1f;
        if (direction.x > DeadZone)
        {
            angle = 0;
        }
        else if (direction.x < -DeadZone)
        {
            angle = 180;
        }
        else if (direction.y > DeadZone)
        {
            angle = 90;
        }
        else if (direction.y < -DeadZone)
        {
            angle = 270;
        }

        return angle;
    }
    public static float Direction2AngleHorizontal(Vector2 direction, float DeadZone)
    {
        float angle = 90f;
        if (direction.x > DeadZone)
        {
            angle = 0;
        }
        else if (direction.x < -DeadZone)
        {
            angle = 180;
        }

        return angle;
    }
}
