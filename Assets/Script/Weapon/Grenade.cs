﻿using UnityEngine;
using System.Collections;

public class Grenade : WeaponBase
{

    public float ForceThrow = 10f;
    private Animator anim = null;
    public void Start()
    {
        element = Reaction.Element.Fire;
        if (owner != null)
        {
            GishControllerNew controller = owner.GetComponentInChildren<GishControllerNew>();
            if (controller != null)
            {
                Aim(controller.Center, LastDirection);
            }
            
        }

    }

    public override void Aim(Vector2 position, Vector2 direction)
    {
        if (aimCurrent == null)
        {
            aimCurrent = CreateAndRotate(AimVisual, position, direction);
            if (aimCurrent != null) anim = aimCurrent.GetComponent<Animator>();
        }
        else
        {
            Rotate(aimCurrent, position, direction);
        }

    }

    public override void DestroyAim()
    {
        anim = null;
        base.DestroyAim();
    }

    public override bool ShotPower()
    {
        shotPower = Mathf.Clamp01(shotPower + speedPower*Time.fixedDeltaTime);
        return shotPower >= 1 ? true : false;
    }

    public override void ShotPowerReset()
    {
        shotPower = 0;
    }

    public override void Shot(Vector2 position, Vector2 direction)
    {
        if (direction.SqrMagnitude() < float.Epsilon) return;

        //DestroyAim();
        GameObject bomb = CreateAndRotate(ShotVisual, position, direction);
        Rigidbody2D rig = bomb.GetComponentInChildren<Rigidbody2D>();
        rig.AddForce(direction.normalized * ForceThrow, ForceMode2D.Impulse);
        DestroyAim();
        //Sender.SendEvent(EventName.NEXT_TEAM, this);
        base.Shot(position, direction);
    }
}
