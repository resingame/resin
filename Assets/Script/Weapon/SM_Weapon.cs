﻿using UnityEngine;
using System.Collections;

public class SM_Weapon : MonoBehaviour {

    public int ShotsNumber = 1;
    public float TimeAfterLastShot = 5f;
    public LayerMask mask;
    public float DeadZone = 0.3f;
    [ComponentSelect]
    [PropertyArgs("Поведение", "Скрипт отвечающий за поведение объекта и его реакцию на внешнее воздействие")]
    public GameObject AimObject = null;
    public GameObject ShotObject = null;
    //public float ShotPower = 1f;
    public Vector2 Position = Vector2.zero;
    [AnimatorParameter(AnimatorParameterAttribute.ParameterType.Bool)]
    [PropertyArgs("Старт выстрела", "Параметр отвечающий за начало анимации выстрела")]
    public int AnimParmStartShot;
    [AnimatorParameter(AnimatorParameterAttribute.ParameterType.Trigger)]
    [PropertyArgs("Конец выстрела", "Параметр отвечающий за конец анимации выстрела")]
    public int AnimParmEndShot;
    [AnimatorParameter(AnimatorParameterAttribute.ParameterType.Int)]
    [PropertyArgs("Количество выстрелов", "Параметр отвечающий за подсчет количества оставшихся выстрелов")]
    public int AnimParmShotNum;

    public static Behaviour OWNER = null;
    //[AnimatorParameter(AnimatorParameterAttribute.ParameterType.Int)]
    //[PropertyArgs("Сила выстрела", "Параметр отвечающий за силу выстрела")]
    //public string AnimParmShotPower = "ShotPower";
    //public Vector2 accurateDirection = Vector2.up;
    private Vector2 _lastDirection = Vector2.up;
    private Vector2 _newDirection = Vector2.up;
    //private int idShotPower = 0;

    public static float TileSize = 2f;

    private Animator animator;


    public bool CanShoot
    {
        get
        {
            if (animator != null)
                return animator.GetInteger(AnimParmShotNum) > 0;
            else
                return false;
        }
    }

    public virtual Vector2 NewDirection
    {
        get { return _newDirection; }
        set
        {
            _newDirection = value;
            if (value.sqrMagnitude <= DeadZone * DeadZone) return;
            _lastDirection = value;
        }
    }
    public virtual Vector2 LastDirection
    {
        get { return _lastDirection; }
        
    }

    public bool StartShot
    {
        set
        {
            if (animator != null)
                animator.SetBool(AnimParmStartShot, value);
        }
    }

    public bool EndShot
    {
        set { if (animator != null) animator.SetTrigger(AnimParmEndShot); }
    }

    public void Reload()
    {
        if (animator != null)
            animator.SetInteger(AnimParmShotNum, ShotsNumber);
    }

    void Awake()
    {
        animator = GetComponentInParent<Animator>();
        if (animator != null)
        {
            Reload();
        }
    }

}
