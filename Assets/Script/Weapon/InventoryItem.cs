﻿using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class InventoryItem:ICloneable
{

    public GameObject weapon = null;
    public int quantity = -1;
    public bool unlimited = true;

    public object Clone()
    {
        InventoryItem item = new InventoryItem();
        item.weapon = weapon;
        item.quantity = quantity;
        item.unlimited = unlimited;
        return item;

    }
}
