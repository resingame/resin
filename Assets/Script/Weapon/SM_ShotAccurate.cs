﻿using UnityEngine;
using System.Collections;
using JetBrains.Annotations;
using KingDOM.Event;

public class SM_ShotAccurate : StateMachineBehaviour
{
    public float TimeAfterLastShot = 5f;
    public bool UseMaxShot = false;

    private SM_Weapon weapon;

    private SM_Weapon GetWeapon(Animator animator)
    {
        if (weapon == null) weapon = animator.GetComponent<SM_Weapon>();
        return weapon;
    }// OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (GetWeapon(animator) != null && weapon.NewDirection.sqrMagnitude > weapon.DeadZone * weapon.DeadZone)
        {
            int hash = weapon.AnimParmShotNum;
            int val = animator.GetInteger(hash);
            val--;
            animator.SetInteger(hash, val);
            weapon.ShotObject.SetActive(true);
            float angle = Vector2.Angle(Vector2.right, weapon.LastDirection);
            WeaponUtility.Rotate(weapon.ShotObject, weapon.Position, angle);
            if (val <= 0) Sender.SendEvent(EventName.TIME_START, this, ParmName.TIME, TimeAfterLastShot);
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    GetWeapon(animator).ShotObject.SetActive(false);
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
