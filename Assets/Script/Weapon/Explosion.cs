﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEditor;
using Random = UnityEngine.Random;

public class Explosion : MonoBehaviour
{

    public float KillZone = 1f;
    public float effectFire = 30f;
    public float BlastWaveForce = 100f;
    public float BlastWaveForceLiquid = 400f;
    public int ParticleSystemImIn = 0;
    public LPFixture ExplosionShape;
    private IntPtr shape;
    public List<Vector2> contacts; 
    

    // Use this for initialization
    void Start () {
        shape = ExplosionShape.GetShape();
        contacts = new List<Vector2>();
        Explose();
	    MakeMine();
	}

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, KillZone * SM_Weapon.TileSize);
        /*Handles.DrawWireDisc(new Vector2(transform.position.x, transform.position.y) // position
                                      , transform.forward                       // normal
                                      , KillZone * SM_Weapon.TileSize);                              // radius
        Handles.DrawWireDisc(new Vector2(transform.position.x+10, transform.position.y) // position
                                      , transform.forward                       // normal
                                      , KillZone * SM_Weapon.TileSize);                              // radius*/
        Gizmos.color = Color.cyan;
        foreach (Vector2 contact in contacts)
        {
            Gizmos.DrawWireSphere(new Vector3(contact.x, contact.y, transform.position.z), 0.2f);
            //Handles.DrawWireDisc(contact, transform.forward, 0.5f);
        }

    }

    private void Explose()
    {
        float killZoneValue = KillZone* SM_Weapon.TileSize;
        RaycastHit2D[] hits = Physics2D.BoxCastAll(transform.position, new Vector2(killZoneValue*2, killZoneValue*2), 0, Vector2.zero);
        Dictionary<Behaviour, float> targets = new Dictionary<Behaviour, float>();
        foreach (RaycastHit2D hit in hits)
        {
            if (hit.collider != null)
            {
                contacts.Add(hit.point);
                Vector2 direction = hit.point - new Vector2(transform.position.x, transform.position.y);
                float distance = direction.magnitude;
                float rate = distance <= killZoneValue ? 1 - distance / killZoneValue : 0;
                //сначала считаем урон от огня
                Behaviour behaivour = hit.transform.GetComponentInParent<Behaviour>();
                float val = effectFire*rate;
                if (behaivour != null)
                {
                    if (targets.ContainsKey(behaivour))
                    {
                        if (targets[behaivour] < val) targets[behaivour] = val;
                    }
                    else
                    {
                        targets.Add(behaivour, val);
                    }
                }
                /*if (behaivour != null)
                {
                    behaivour.Effect(Reaction.Element.Fire, effectFire * rate);
                }*/
                // теперь расчитываем толчок от взрывной волны
                if (hit.rigidbody != null)
                    hit.rigidbody.AddForce(direction.normalized * BlastWaveForce * rate, ForceMode2D.Impulse);
            }
        }
        foreach (KeyValuePair<Behaviour, float> pair in targets)
        {
            pair.Key.Effect(Reaction.Element.Fire, pair.Value);
        }
    }
    void MakeMine()
    {
        Transform transform = this.transform;
        LPManager lpman = LiquidManager.LpManager;
        IntPtr partsysptr = lpman.ParticleSystems[ParticleSystemImIn].GetPtr();
        IntPtr particlesPointer = LPAPIParticleSystems.GetParticlesInShape(partsysptr
                                                                           , shape, transform.position.x, transform.position.y
                                                                           , transform.rotation.eulerAngles.z);

        int[] particlesArray = new int[1];
        Marshal.Copy(particlesPointer, particlesArray, 0, 1);
        int foundNum = particlesArray[0];


        if (foundNum > 0)
        {
            int[] Indices = new int[foundNum + 1];
            Marshal.Copy(particlesPointer, Indices, 0, foundNum + 1);

            LPAPIParticles.ExplodeSelectedParticles(partsysptr, Indices, transform.position.x, transform.position.y, BlastWaveForceLiquid);
        }
        //GetComponent<LPBody>().Delete();
    }
}
