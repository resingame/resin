﻿using UnityEngine;
using System.Collections;
using KingDOM;

public class MainInputController : MonoBehaviour
{

    public CameraController CameraController = null;
    
    public string changeCameraPlane = "plane";

    private bool isOk = false;
    // Use this for initialization
	void Start ()
	{
	    isOk = true;
	    isOk = KingUtil.CheckNotNull(CameraController, "Не удалось найти контроллер для камеры.", isOk, this);
	}
	
	// Update is called once per frame
	void Update () {
	    if (!isOk) return;
	    if (Input.GetButtonDown(changeCameraPlane)) CameraController.AllMap = !CameraController.AllMap;
	}
}
