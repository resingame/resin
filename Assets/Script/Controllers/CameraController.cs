﻿using UnityEngine;
using System.Collections;
using KingDOM;
using KingDOM.Event;

public class CameraController : MonoBehaviour
{

    public Camera view = null;
    public Camera metaball = null;
    public CameraSize targetAllMap = null;
    public CameraSize targetPerson = null;
    public CameraSize targetBonus = null;
    public GameObject[] backs = null;
    [Range(3, 20)]
    public int sizePersonInTile = 10;
    public int sizeBonusInTile = 10;
    public FollowMe followMe;
    public MetaballCameraController MetaballController;

    private bool isOk = false;
    private CameraSize currentTarget = null;
    private FollowMe personFollowMe = null;
    private FollowMe bonusFollowMe = null;
    //private CameraSize lastTarget = null;
    //private Resolution lastResolution;
    private float lastWidth = 0;
    private float lastHeight = 0;

    public bool AllMap
    {
        get { return currentTarget == targetAllMap; }
        set
        {
            if (currentTarget == targetBonus) return;
            if (value)
            {
                currentTarget = targetAllMap;
                followMe.Target = targetAllMap.transform;
            }
            else
            {
                currentTarget = targetPerson;
                followMe.Target = targetPerson.transform;
            }
        }
    }
    // Use this for initialization
	void Start ()
	{
	    isOk = true;
        isOk = KingUtil.CheckNotNull(view, "Не удалось обнаружить основную камеру", isOk, this);
        isOk = KingUtil.CheckNotNull(metaball, "Не удалось обнаружить камеру для отрисовки жидкости", isOk, this);
        isOk = KingUtil.CheckNotNull(targetAllMap, "Не удалось обнаружить камеру для отрисовки жидкости", isOk, this);
        isOk = KingUtil.CheckNotNull(metaball, "Не удалось обнаружить камеру для отрисовки жидкости", isOk, this);
        isOk = KingUtil.CheckNotNull(followMe, "Не удалось обнаружить скрипт следования за целью.", isOk, this);
        isOk = KingUtil.CheckNotNull(MetaballController, "Не удалось обнаружить скрипт управления камерой жидкости.", isOk, this);

        Init();
        isOk = KingUtil.CheckNotNull(personFollowMe, "Не удалось обнаружить скрипт следования за активным войном.", isOk, this);
    }

    // Update is called once per frame
    void Update () {
        if (!isOk) return;
        if (lastWidth != view.pixelWidth || lastHeight != view.pixelHeight)
        {
            lastWidth = view.pixelWidth;
            lastHeight = view.pixelHeight;
            MetaballController.Generate(view);
            ResizeBackground();
        }
        if (currentTarget.OrtographicSize != view.orthographicSize)
        {
            view.orthographicSize = Mathf.Lerp(view.orthographicSize, currentTarget.OrtographicSize, Time.deltaTime);
            MetaballController.Generate(view);
            ResizeBackground();
        }
        if (metaball.orthographicSize != view.orthographicSize)
        {
            metaball.orthographicSize = view.orthographicSize;
            MetaballController.Generate(view);
            ResizeBackground();
        }

        //Debug.Log(string.Format("View ({0}:{1}) - Screen({2}:{3})", view.pixelWidth, view.pixelHeight, Screen.width, Screen.height));
        //Debug.Log(string.Format("View ({0}:{1}) - Metaball({2}:{3})", view.pixelWidth, view.pixelHeight, metaball.pixelWidth, metaball.pixelHeight));
    }

    void OnDestroy()
    {
        Sender.RemoveEvent(EventName.WARRIOR_READY, hnWarriorReady);
        Sender.RemoveEvent(EventName.BONUS_CREATE, hnBonusCreate);
        Sender.RemoveEvent(EventName.BONUS_GENERATE_END, hnBonusGenerateEnd);
    }

    public void Resize(int sizeMap, float tileSize)
    {
        SetCamera(view, sizeMap, tileSize);
        SetCamera(metaball, sizeMap, tileSize);
        ResizeBackground();
        targetAllMap.OrtographicSize = tileSize*(sizeMap/2 + 1);
        targetPerson.OrtographicSize = tileSize*sizePersonInTile;
        targetBonus.OrtographicSize = tileSize*sizeBonusInTile;
    }

    private void Init()
    {
        if (!isOk) return;
        MetaballCameraController controller = metaball.GetComponent<MetaballCameraController>();
        if (controller != null) controller.Generate(view);
        currentTarget = targetAllMap;
        //lastTarget = targetAllMap;
        personFollowMe = targetPerson.GetComponent<FollowMe>();
        bonusFollowMe = targetBonus.GetComponent<FollowMe>();
        //lastResolution = Screen.currentResolution;
        lastHeight = Camera.main.pixelHeight;
        lastWidth = Camera.main.pixelWidth;
        Sender.AddEvent(EventName.WARRIOR_READY, hnWarriorReady);
        Sender.AddEvent(EventName.BONUS_CREATE, hnBonusCreate);
        Sender.AddEvent(EventName.BONUS_GENERATE_END, hnBonusGenerateEnd);
    }

    private void SetCamera(Camera cam, int sizeMap, float tileSize)
    {
        cam.transform.position = new Vector3(transform.position.x, transform.position.y, -(cam.farClipPlane - cam.nearClipPlane) / 2 - cam.nearClipPlane);
        cam.orthographicSize = tileSize * ((sizeMap + 2) / 2);
    }

    private void ResizeBackground()
    {
        foreach (var back in backs)
        {
            SetBackGround(back);
        }
    }

    private void SetBackGround(GameObject back)
    {
        Camera cam = Camera.main;
        //back.transform.position = new Vector3(transform.position.x, transform.position.y, cam.nearClipPlane + 1f);
        back.transform.localScale = new Vector3(cam.orthographicSize * 2 * Screen.width / Screen.height, cam.orthographicSize * 2, 1);
    }

    private void hnWarriorReady(SimpleEvent evnt)
    {
        if (!isOk) return;
        if (evnt == null) return;

        if (evnt.ExistParm<GishControllerNew>(ParmName.WARRIOR))
        {
            GishControllerNew gish = evnt.GetParm<GishControllerNew>(ParmName.WARRIOR);
            personFollowMe.Target = gish.CentralTransform;
            currentTarget = targetPerson;
            followMe.Target = currentTarget.transform;
        }
    }
    private void hnBonusCreate(SimpleEvent evnt)
    {
        if (!isOk) return;
        if (evnt == null || evnt.target == null || !(evnt.target is Component)) return;

        //lastTarget = currentTarget;
        bonusFollowMe.Target = (evnt.target as Component).transform;
        currentTarget = targetBonus;
        followMe.Target = currentTarget.transform;
    }
    private void hnBonusGenerateEnd(SimpleEvent evnt)
    {
        if (!isOk) return;
        if (evnt == null || evnt.target == null || !(evnt.target is Component)) return;
        currentTarget = AllMap? targetAllMap: targetPerson;
        followMe.Target = currentTarget.transform;
    }

}
