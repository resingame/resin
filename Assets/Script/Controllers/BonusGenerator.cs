﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using KingDOM;
using KingDOM.Event;

public class BonusGenerator : MonoBehaviour
{
    public enum State
    {
        Hold,
        Create,
    }
    //public LayerMask maskEmpty = -1;
    //public LayerMask maskConflict = -1;
    //public float CheckRadiusEmpty = 1f;
    //public float CheckRadiusConflict = 1f;
    public BonusModel[] bonuses = new BonusModel[0];
    private int startX = 0;
    private int startY = 0;
    private List<BonusModel> bonusGenerations;
    private LevelManager levelManager;
    private const float  TOLERANCE = 0.1f;
    // Use this for initialization
	void Awake ()
	{
        bonusGenerations = new List<BonusModel>();
	    if (levelManager == null) levelManager = FindObjectOfType<LevelManager>();
        Sender.AddEvent(EventName.BONUS_GENERATE_START, hnBonusGenerateStart);
        Sender.AddEvent(EventName.BONUS_READY, hnBonusReady);

    }

    void OnDestroy()
    {
        Sender.RemoveEvent(EventName.BONUS_GENERATE_START, hnBonusGenerateStart);
        Sender.RemoveEvent(EventName.BONUS_READY, hnBonusReady);
    }

    void hnBonusGenerateStart(SimpleEvent evnt)
    {
        CreateListGeneration();
    }
    void hnBonusReady(SimpleEvent evnt)
    {
        if (bonusGenerations.Count > 0)
        {
            CreateBonus();
        }
        else
        {
            GenerationComplete();
        }
    }

    void CreateListGeneration()
    {
        bonusGenerations = new List<BonusModel>();
        foreach (BonusModel bonus in bonuses)
        {
            BonusModel b = bonus.Clone() as BonusModel;
            b.range.RandomInt();
            if (b.range.value > 0 && b.probabilityOccurance > Random.value)
                bonusGenerations.Add(b);
        }
        if (bonusGenerations.Count <= 0)
        {
            GenerationComplete();
        }
        else
        {
            CreateBonus();
        }
    }

    void CreateBonus()
    {

        Vector2 idxs = Vector2.zero;
        BonusModel bonus = null;
        do
        {
            bonus = GetNextBonus();
            if (bonus == null)
            {
                GenerationComplete();
                return;
            }
            idxs = GetIdx(bonus);
            if (idxs.x >= 0 && idxs.y >= 0)
            {
                break;
            }
            bonusGenerations.Remove(bonus);
        } while (true);
        
        GameObject tile = GameObject.Instantiate(bonus.tile);
        tile.transform.parent = transform;
        tile.transform.position = levelManager.Idxs2Point((int)idxs.x, (int)idxs.y);
    }

    BonusModel GetNextBonus()
    {
        BonusModel bonus = null;
        do
        {
            if (bonusGenerations.Count <= 0) return null;
            bonus = bonusGenerations[Random.Range(0, bonusGenerations.Count - 1)];
            if (bonus.range.value <= 0)
            {
                bonusGenerations.RemoveAt(0);
                bonus = null;
                continue;
            }
            else
            {
                bonus.range.value--;
                break;
            }


        } while (true);

        return bonus;
    }

    Vector2 GetIdx(BonusModel model)
    {
        int sizeMap = levelManager.Match.SizeMap;
        Vector2 idxs = new Vector2(Random.Range(0, sizeMap - 1), Random.Range(0, sizeMap - 1));
        int startX = (int) idxs.x;
        int startY = (int) idxs.y;
        int x = startX;
        int y = startY;
        while (!EmptyTile(x, y, model))
        {
            x++;
            if (x >= sizeMap)
            {
                x = 0;
                y++;
                if (y >= sizeMap) y = 0;
            }

            if (x==startX && y == startY) return new Vector2(-1,-1);
        }
        return new Vector2(x, y);
    }

    bool EmptyTile(int x, int y, BonusModel model)
    {
        Vector3 pos = levelManager.Idxs2Point(x, y);
        Vector2 pos2D = new Vector2(pos.x, pos.y);
        RaycastHit2D hit = Physics2D.CircleCast(pos2D, levelManager.TileSize*model.CheckDiameterEmpty/2 - TOLERANCE, Vector2.zero, 0, model.maskEmpty);
        if (hit.collider != null)
        {
            return false;
        }
        hit = Physics2D.CircleCast(pos2D, levelManager.TileSize * model.CheckDiameterConflict/2 - TOLERANCE, Vector2.zero, 0, model.maskConflict);
        if (hit.collider != null)
        {
            return false;
        }

        RaycastHit2D[] hits = Physics2D.RaycastAll(pos2D, Vector2.down, levelManager.TileSize*100,
            model.maskConflict | model.maskEmpty);
        foreach (RaycastHit2D hit2D in hits)
        {
            if (hit2D.collider != null)
            {
                int layer = hit2D.collider.gameObject.layer;
                if (KingUtil.LayerInMask(layer, model.maskEmpty)) return true;
                if (KingUtil.LayerInMask(layer, model.maskConflict)) return false;
            }
        }
        return false;
    }

    void GenerationComplete()
    {
        bonusGenerations = new List<BonusModel>();
        Sender.SendEvent(EventName.BONUS_GENERATE_END, this);
    }
}
