﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class SnchroOrtographicSize : MonoBehaviour {

    public Camera FromCamera = null;

    private Camera cam = null;

    void Awake()
    {
        cam = GetComponent<Camera>();
    }

    void Update()
    {
        if (FromCamera != null && FromCamera.orthographicSize != cam.orthographicSize)
        {
            cam.orthographicSize = FromCamera.orthographicSize;
        }
    }
}
