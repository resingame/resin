﻿using UnityEngine;
using System.Collections;

public class DestroyByTime : MonoBehaviour
{
    public GameObject target;
    public float TimeWait = 1f;
    private float timeLeft = 0f;
    // Use this for initialization
	void Start ()
	{
	    if (target == null) target = gameObject;
        timeLeft = TimeWait;
	}
	
	// Update is called once per frame
	void Update () {
	    if (timeLeft > 0)
	    {
	        timeLeft -= Time.deltaTime;
	    }
	    else
	    {
	        Destroy(target);
	    }
	}
}
