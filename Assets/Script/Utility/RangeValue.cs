﻿using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class RangeValue
{

    public float min;
    public float max;
    private float _value;

    public float value
    {
        get { return _value; }
        set { _value = Mathf.Clamp(value, min, max); }
    }

    public float middle
    {
        get { return (min + max) / 2; }
    }

    public RangeValue():this(0,1)
    {
    }

    public RangeValue(float min, float max, float newValue)
    {
        this.min = min;
        this.max = max;
        _value = min;
        value = newValue;
    }

    public RangeValue(float min, float max)
    {
        this.min = min;
        this.max = max;
        this._value = min;
    }

    public static RangeValue one
    {
        get
        {
            RangeValue r = new RangeValue();

            r.min = 0f;
            r.max = 1f;
            r.value = 0f;
            return r;
        }
    }

    public float Random(bool setValue = true)
    {
        float value = UnityEngine.Random.Range(min, max);
        if (setValue) this.value = value;
        return value;
    }
    public int RandomInt(bool setValue = true)
    {
        int value = UnityEngine.Random.Range((int)min,(int)max);
        if (setValue) this.value = value;
        return value;
    }

}
