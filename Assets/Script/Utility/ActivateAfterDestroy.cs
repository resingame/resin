﻿using UnityEngine;
using System.Collections;

public class ActivateAfterDestroy : MonoBehaviour
{
    public GameObject NeedActivate = null;
    public bool SetActive = true;
    // Use this for initialization
	void OnDestroy ()
	{
	    if (NeedActivate != null)
	    {
	        NeedActivate.transform.position = transform.position;
	        NeedActivate.transform.rotation = transform.rotation;
            NeedActivate.SetActive(SetActive);
	    }
	}
	
}
