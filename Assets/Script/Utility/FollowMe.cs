﻿using UnityEngine;
using System.Collections;

public class FollowMe : MonoBehaviour
{
    public Transform Target = null;
    public bool Smoothing = true;
    //public Transform LookObject = null;
    //public bool KeepDistance = true;
    [PropertyBase]
    [PropertyArgs(hideIfEmpty= "Smoothing")]
    public float TimeMoving = 0.5f;
    public Vector3 MoveMask = Vector3.one;
    public Vector3 RotateMask = Vector3.one;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Target != null)
        {
            if (Smoothing)
            {
                /*float distance = 0f;

                if (LookObject != null)
                {
                    distance = (transform.position - LookObject.position).magnitude;
                }*/
                UpdatePosition(Vector3.Slerp(transform.position, Target.position, Time.deltaTime / TimeMoving));
                UpdateRotation(Quaternion.Slerp(transform.rotation, Target.rotation, Time.deltaTime / TimeMoving).eulerAngles);
                /*if (LookObject != null && KeepDistance)
                {
                    Vector3 direction = transform.position - LookObject.position;
                    if (distance > direction.magnitude)
                        transform.position = LookObject.position + direction.normalized * distance;
                }*/
            }
            else
            {
                UpdatePosition(Target.position);
                UpdateRotation(Target.eulerAngles);
            }
        }
    }

    void UpdatePosition(Vector3 position)
    {
        Vector3 old = transform.position;
        transform.position = new Vector3(old.x * (1 - MoveMask.x) + position.x * MoveMask.x, old.y * (1 - MoveMask.y)+position.y* MoveMask.y, old.z * (1 - MoveMask.z) + position.z * MoveMask.z );
    }

    void UpdateRotation(Vector3 rotation)
    {
        Vector3 old = transform.eulerAngles;
        transform.eulerAngles = new Vector3(old.x * (1 - RotateMask.x) + rotation.x * RotateMask.x, old.y * (1 - RotateMask.y) + rotation.y * RotateMask.y, old.z * (1 - RotateMask.z) + rotation.z * RotateMask.z);

    }
}


