﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomPropertyDrawer(typeof(RangeValue))]
public class RangeValueDrawer : PropertyDrawer {

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializedProperty propName = property.FindPropertyRelative("min");
        EditorGUI.BeginProperty(position, label, property);

        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
        // Don't make child fields be indented
        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // Calculate rects
        Rect minRect = new Rect(position.x, position.y, 45, position.height);
        Rect labelRect = new Rect(position.x + 50, position.y, 15, position.height);
        Rect maxRect = new Rect(position.x + 70, position.y, 45, position.height);

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        EditorGUI.PropertyField(minRect, propName, GUIContent.none);
        EditorGUI.LabelField(labelRect, new GUIContent("-"));
        SerializedProperty p = property.FindPropertyRelative("max");
        EditorGUI.PropertyField(maxRect, p, GUIContent.none);

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUIUtility.singleLineHeight;
    }
}
