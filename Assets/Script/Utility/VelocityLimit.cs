﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class VelocityLimit : MonoBehaviour
{

    public float SpeedLimit = 1f;
    private Rigidbody2D rigid;
    // Use this for initialization
	void Start ()
	{
	    rigid = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
	    rigid.velocity = rigid.velocity.magnitude > SpeedLimit ? rigid.velocity.normalized*SpeedLimit : rigid.velocity;
	}
}
