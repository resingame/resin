﻿using UnityEngine;
using System.Collections;

public class DeactivateByTime : MonoBehaviour {

    public float TimeWait = 1f;
    private float timeLeft = 0f;
    // Use this for initialization
    void Start()
    {
        timeLeft = TimeWait;
    }

    // Update is called once per frame
    void Update()
    {
        if (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}
