﻿using System;
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class LiquidUtility {

    public static int[] GetArray(IntPtr arrayPtr)
    {
        int[] Indices = new int[0];
        int[] particlesArray = new int[1];
        Marshal.Copy(arrayPtr, particlesArray, 0, 1);
        int foundNum = particlesArray[0];


        if (foundNum > 0)
        {
            Indices = new int[foundNum + 1];
            Marshal.Copy(arrayPtr, Indices, 0, foundNum + 1);

        }
        return Indices;
    }
}
