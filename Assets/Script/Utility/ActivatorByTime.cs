﻿using UnityEngine;
using System.Collections;

public class ActivatorByTime : MonoBehaviour
{

    public MonoBehaviour Target = null;
    public float TimeWait = 1f;
    public bool SetValue = false;
    private float timeLeft = 0f;
    // Use this for initialization
    void Start()
    {
        timeLeft = TimeWait;
    }

    // Update is called once per frame
    void Update()
    {
        if (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
        }
        else
        {
            Target.enabled = SetValue;
            enabled = false;
        }
    }
}
