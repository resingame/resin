﻿using UnityEngine;
using System.Collections;
using KingDOM.Event;

public class SMEventGenerator : StateMachineBehaviour
{
    public enum Reaction
    {
        None,
        OnStateEnter,
        OnStateUpdate,
        OnStateExit,
        OnStateMove,
        OnStateIK,
    }
    public Reaction reaction = Reaction.None;
    [ConstSelect(typeof(EventName))]
    public string Event;

    public bool IsHierarchical = false;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (reaction == Reaction.OnStateEnter) Send(animator);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (reaction == Reaction.OnStateUpdate) Send(animator);
    }

    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (reaction == Reaction.OnStateExit) Send(animator);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (reaction == Reaction.OnStateMove) Send(animator);

    }

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (reaction == Reaction.OnStateIK) Send(animator);

    }
    private void Send(Animator animator)
    {
        if (string.IsNullOrEmpty(Event)) return;
        if (IsHierarchical)
        {
            Sender.SendEventHierarchy(Event, animator);
        }
        else
        {
            Sender.SendEvent(Event, animator);
        }
    }
}
