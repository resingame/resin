﻿using UnityEngine;
using System.Collections;
using KingDOM.Event;

public class EventGenerator : MonoBehaviour
{

    public enum Reaction
    {
        None,
        Awake,
        Start,
        OnEnabled,
        Update,
        FixedUpdate,
        OnDisable,
        OnDestroy
    }

    public Reaction reaction = Reaction.None;
    [ConstSelect(typeof(EventName))]
    public string Event;

    public bool IsHierarchical = false;
    public Component Source = null;

    void Awake()
    {
        if (reaction == Reaction.Awake) Send();
    }
    // Use this for initialization
    void Start()
    {
        if (reaction == Reaction.Start) Send();
    }

    void OnEnable()
    {
        if (reaction == Reaction.OnEnabled) Send();
    }

    // Update is called once per frame
    void Update()
    {
        if (reaction == Reaction.Update) Send();
    }

    public void FixedUpdate()
    {
        if (reaction == Reaction.FixedUpdate) Send();
    }

    public void OnDisable()
    {
        if (reaction == Reaction.OnDisable) Send();
    }

    public void OnDestroy()
    {
        if (reaction == Reaction.OnDestroy) Send();
    }

    private void Send()
    {
        if (string.IsNullOrEmpty(Event)) return;
        if (IsHierarchical)
        {
            Sender.SendEventHierarchy(Event, Source == null ? this : Source);
        }
        else
        {
            Sender.SendEvent(Event, Source == null ? this : Source);
        }
    }
}
