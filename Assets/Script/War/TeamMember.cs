﻿using UnityEngine;
using System.Collections;

public class TeamMember : MonoBehaviour
{

    public string name = "Body";
    public bool isLive = true;
    public Team team = null;
    private InputGishNew input;

    public GishControllerNew Gish
    {
        get { return input.gish; }
    }
    // Use this for initialization
	void Awake ()
	{
        input = GetComponentInChildren<InputGishNew>();
    }

    void Update()
    {
        
    }
	
    public void SetActive(bool val)
    {
        if (input != null)
        {
            input.enabled = val;
        }
    }

    public void EndTimer()
    {
        if (input != null)
        {
            input.gish.InertiaWorkFlag = true;
            SetActive(false);
        }
    }

    public void SetWeapon(GameObject weapon)
    {
        input.gish.SetNewWeapon(weapon);
    }

}
