﻿using UnityEngine;
using System.Collections;
using KingDOM;
using KingDOM.Event;
using UnityEngine.UI;

public class Referee : MonoBehaviour
{

    public Match match = null;
    public Text WeaponNameText = null;

    private bool isOk = false;
    private int curIdx = 0;
    private bool isFirstMove = false;
    private TeamMember currentWarrior = null;
    private bool checkWinner = false;
    private bool isPressed = false;

    // Use this for initialization
    void OnEnable ()
    {
        Sender.AddEvent(EventName.MAP_CREATE_END, hnEndCreateMap);
        Sender.AddEvent(EventName.NEXT_TEAM, hnNextTeam);
        Sender.AddEvent(EventName.WARRIOR_READY, hnWarriorReady);
        Sender.AddEvent(EventName.NEXT_WARRIOR, hnNextWarrior);
        Sender.AddEvent(EventName.TIME_END, hnEndTime);
        Sender.AddEvent(EventName.WARRIOR_GET_DAMAGE, hnWarriorGetDamage);
        Sender.AddEvent(EventName.WARRIOR_DIE, hnWarriorDie);
        Sender.AddEvent(EventName.GAME_END, hnGameEnd);
        Sender.AddEvent(EventName.BONUS_GENERATE_START, hnBonusCreateStart);
        Sender.AddEvent(EventName.BONUS_GENERATE_END, hnBonusCreateEnd);
    }

    void OnDisable()
    {
        Sender.RemoveEvent(EventName.MAP_CREATE_END, hnEndCreateMap);
        Sender.RemoveEvent(EventName.NEXT_TEAM, hnNextTeam);
        Sender.RemoveEvent(EventName.WARRIOR_READY, hnWarriorReady);
        Sender.RemoveEvent(EventName.NEXT_WARRIOR, hnNextWarrior);
        Sender.RemoveEvent(EventName.TIME_END, hnEndTime);
        Sender.RemoveEvent(EventName.WARRIOR_GET_DAMAGE, hnWarriorGetDamage);
        Sender.RemoveEvent(EventName.WARRIOR_DIE, hnWarriorDie);
        Sender.RemoveEvent(EventName.GAME_END, hnGameEnd);
        Sender.RemoveEvent(EventName.BONUS_GENERATE_START, hnBonusCreateStart);
        Sender.RemoveEvent(EventName.BONUS_GENERATE_END, hnBonusCreateEnd);
    }

    void Start()
    {
        isOk = true;
        isOk = KingUtil.CheckNotNull(match, "Не заданы настройки для поединка", isOk, this);
        isFirstMove = true;
        Sender.SendEvent(EventName.MAP_CREATE_START, this, ParmName.MATCH, match);
    }
	
	// Update is called once per frame
	void Update ()
	{
	    float changeWeapon = Input.GetAxisRaw("Next Weapon");
        if (isPressed && changeWeapon != 0)return;
	    if (changeWeapon == 0) isPressed = false;
	    if (changeWeapon > float.Epsilon)
	    {
	        currentWarrior.team.inventory.GetNext();
            UpdateWeapon();
            isPressed = true;
	    }
	    else
	    {
	        if (changeWeapon < -float.Epsilon)
	        {
	            currentWarrior.team.inventory.GetPrevious();
                UpdateWeapon();
                isPressed = true;
	        }
        }
	}

    void hnEndCreateMap(SimpleEvent evnt)
    {
        Sender.SendEvent(EventName.NEXT_TEAM, this);
    }

    void hnNextTeam(SimpleEvent evnt = null)
    {
        if (currentWarrior != null) currentWarrior.SetActive(false);
        if (isFirstMove)
        {
            SelectStartTeam();
            Sender.SendEvent(EventName.BONUS_GENERATE_END, this);
        }
        else
        {
            if (checkWinner && CountActiveComand() <= 1)
            {
                Sender.SendEvent(EventName.GAME_END, this);
                return;
            }
            checkWinner = false;

            NextTeamSet();
            Sender.SendEvent(EventName.BONUS_GENERATE_START, this);
        }
    }

    void hnWarriorReady(SimpleEvent evnt)
    {
    }

    void hnBonusCreateStart(SimpleEvent evnt)
    {
    }

    void hnBonusCreateEnd(SimpleEvent evnt)
    {
        Sender.SendEvent(EventName.NEXT_WARRIOR, this);
    }

    void hnNextWarrior(SimpleEvent evnt)
    {
        if (isFirstMove)
        {
            isFirstMove = false;
            currentWarrior = match.teams[curIdx].Current;
            UpdateWeapon();
        }
        else
        {
            currentWarrior = match.teams[curIdx].Next;
        }
        currentWarrior.SetActive(true);
        if (currentWarrior.team != null && currentWarrior.team.inventory != null &&
            currentWarrior.team.inventory.Current != null)
        {
            UpdateWeapon();
        }
        Sender.SendEvent(EventName.TIME_START, this);
        Sender.SendEvent(EventName.WARRIOR_READY, this, ParmName.WARRIOR, currentWarrior.Gish);
    }

    void UpdateWeapon()
    {
        currentWarrior.SetWeapon(currentWarrior.team.inventory.Current.weapon);
        if (WeaponNameText != null) WeaponNameText.text = currentWarrior.team.inventory.Current.weapon.name;
    }

    void hnEndTime(SimpleEvent evnt)
    {
        currentWarrior.EndTimer();
        hnNextTeam();
    }

    void hnWarriorGetDamage(SimpleEvent evnt)
    {
        Component c = evnt.target as Component;
        if (c && c.gameObject == currentWarrior.gameObject)
            hnNextTeam();
    }

    void hnWarriorDie(SimpleEvent evnt)
    {
        checkWinner = true;
    }
    
    void hnGameEnd(SimpleEvent evnt)
    {
        endGame();
    }

    void SelectStartTeam()
    {
        curIdx = Mathf.RoundToInt(Random.value * (match.teams.Length - 1));
        curIdx = GetActiveIdx();
        if (curIdx >= 0)
        {
            match.teams[curIdx].Current.SetActive(true);
        }
    }

    void NextTeamSet()
    {
        curIdx++;
        if (curIdx >= match.teams.Length) curIdx = 0;
        int idx = GetActiveIdx();
        if (idx < 0)
        {
            endGame();
            return;
        }
        curIdx = idx;
    }

    int GetActiveIdx()
    {
        int startIdx = curIdx;
        while (!match.teams[curIdx].InGame)
        {
            curIdx++;
            curIdx = curIdx >= match.teams.Length ? 0 : curIdx;
            // Прошли полный цикл и все команды вне игры.
            if (curIdx == startIdx) return -1;
        }
        return curIdx;
    }

    void endGame()
    {
        enabled = false;
    }

    int CountActiveComand()
    {
        int res = 0;
        foreach (Team team in match.teams)
        {
            if (team.InGame) res++;
        }
        return res;
    }

    bool GameComplete()
    {
        return CountActiveComand() <= 1;
    }
}
