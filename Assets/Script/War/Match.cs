﻿using UnityEngine;
using System.Collections;
using KingDOM;
using UnityEngine.Assertions.Must;

public class Match : MonoBehaviour
{

    [Range(10, 50)]
    public int SizeMap = 20;

    public LevelScheme scheme;
    public TileModel[] TilesSource = new TileModel[0];
    public Team[] teams = new Team[2];
    public int TeamSize = 3;

    private bool isOk = false;
    private int curIdx = 0;

    // Use this for initialization
    void Awake()
    {

        isOk = (TilesSource != null && TilesSource.Length > 0) ||
               KingUtil.CheckFailed("Не задана схема построения уровня", this);

        InventoryTemplate template = GetComponent<InventoryTemplate>();
        if (template != null)
        {
            foreach (Team team in teams)
            {
                team.inventory = new Inventory();
                team.inventory.Init();
                foreach (InventoryItem item in template.items)
                {
                    team.inventory.items.Add(item.weapon.name, item.Clone() as InventoryItem);
                }
                
            }
        }
    }


}
