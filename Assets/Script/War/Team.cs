﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

[Serializable]
public class Team {

    public GameObject memberPrefab;
    public Inventory inventory;
    public List<TeamMember> members = new List<TeamMember>();
    public string name;
    public Color color;
    public Sprite icon;
    private int idx = 0;

    public TeamMember Add(string name, int energy = 0)
    {
        if (memberPrefab == null) return null;
        GameObject go = GameObject.Instantiate(memberPrefab);
        TeamMember tm = go.GetComponentInChildren<TeamMember>();
        if (tm == null)
        {
            tm = go.AddComponent<TeamMember>();
        }
        tm.name = name;
        if (energy > 0)
        {
            Stamina r = tm.GetComponent<Stamina>();
            if (r != null)
            {
                r.Energy = energy;
            }
        }
        tm.SetActive(false); //деактивируем всех участников команды, судья назначит кому ходить первым.
        tm.team = this;
        Indicator indicator = go.GetComponentInChildren<Indicator>();
        if (indicator != null)
        {
            indicator.Init(color);
        }
        members.Add(tm);
        return tm;
    }
    public TeamMember Next
    {
        get
        {
            if (members == null || members.Count == 0) return null;
            idx = getNextIdx();
            if (idx <0) return null;
            return members[idx];
        }
    }
    public TeamMember Current
    {
        get
        {
            if (members == null || idx >= members.Count) return null;
            return members[idx];
        }
    }

    public bool InGame
    {
        get {
            foreach (TeamMember member in members)
            {
                if (member != null && member.isLive) return true;
            }
            return false;
        }
    }

    private int getNextIdx()
    {
        int start = idx;
        int res = idx;
        do
        {
            res++;
            if (res >= members.Count) res = 0;
            if (res == start) return idx;
        } while (members[res] == null || !members[res].isLive);

        return res;
    }

}
