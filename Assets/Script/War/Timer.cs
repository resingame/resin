﻿using UnityEngine;
using System.Collections;
using KingDOM;
using KingDOM.Event;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    public int TimePeriod = 30;
    public float timeLeft = 0f;
    public Text text = null;

    private bool isOk = false;
    private bool sendEvent = false;

    // Use this for initialization
    void Awake ()
    {
        isOk = TimePeriod > 5 || KingUtil.CheckFailed("Время хода должно быть больше 5 секунд.", this);
        if (text == null) text = GetComponentInChildren<Text>();
        if (isOk)
        {
            Sender.AddEvent(EventName.TIME_START, hnTimerStart);
            Sender.AddEvent(EventName.BONUS_GENERATE_START, 100, hnBonusGenerateStart);
            Sender.AddEvent(EventName.BONUS_GENERATE_END, 100, hnBonusGenerateEnd);
        }
    }

    void OnDestroy()
    {
        if (isOk)
        {
            Sender.RemoveEvent(EventName.TIME_START, hnTimerStart);
            Sender.RemoveEvent(EventName.BONUS_GENERATE_START, 100, hnBonusGenerateStart);
            Sender.RemoveEvent(EventName.BONUS_GENERATE_END, 100, hnBonusGenerateEnd);
        }
    }
	
	// Update is called once per frame
	void Update () {
	    if (timeLeft > 0)
	    {
	        timeLeft -= Time.deltaTime;
	        if (timeLeft <= 0)
	        {
                timeLeft = 0;
                if (sendEvent) Sender.SendEvent(EventName.TIME_END, this);
	        }
	        if (text != null)
	        {
	            if (!text.enabled) text.enabled = true;
	            text.text = Mathf.RoundToInt(timeLeft).ToString();
	        }
	    }
	    else
	    {
	        if (text != null && text.enabled)
	        {
	            text.enabled = false;
	        }
	    }
	}

    void hnTimerStart(SimpleEvent evnt)
    {
        float setTimer = TimePeriod;
        if (evnt.ExistParm<float>(ParmName.TIME)) setTimer = evnt.GetParm<float>(ParmName.TIME);
        timeLeft = setTimer;
    }

    void hnBonusGenerateStart(SimpleEvent evnt)
    {
        sendEvent = false;
        timeLeft = 0;
    }

    void hnBonusGenerateEnd(SimpleEvent evnt)
    {
        sendEvent = true;
    }
}
