﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CircleController : MonoBehaviour
{

    public float forceStrength = 1000;
    public Collider2D collider = null;
    private bool isAction = false;
    // Use this for initialization
	void Start ()
	{
	    collider = GetComponentInChildren<Collider2D>();
        isAction = false;
    }
	
	// Update is called once per frame
	void Update ()
	{
        
        if (isAction) return;
	    if (Input.GetKeyDown(KeyCode.RightArrow))
	    {
	        collider.attachedRigidbody.AddForce(Vector2.right * forceStrength, ForceMode2D.Impulse);
	        isAction = true;
	    }
	    if (Input.GetKeyDown(KeyCode.UpArrow))
	    {
	        collider.attachedRigidbody.AddForce(new Vector2(1,1) * forceStrength, ForceMode2D.Impulse);
	        isAction = true;
	    }


    }
}
