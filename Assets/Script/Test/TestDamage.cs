﻿using UnityEngine;
using System.Collections;

public class TestDamage : MonoBehaviour
{

    public float MaxVelocity = 0f;
    public Vector2 ActionVector = Vector2.zero;
    public Vector2 ActionPosition = Vector2.zero;
    private Texture2D point;
    private DrawLine line = null;
    private Rigidbody2D rigid;
    private Vector2 lastSpeed = Vector2.zero;

    // Use this for initialization
    void Awake () {
	    point = Texture2D.whiteTexture;
        rigid = GetComponent<Rigidbody2D>();
        lastSpeed = Vector2.zero;

    }

    // Update is called once per frame
    /*void FixedUpdate () {
        if (rigid.velocity.sqrMagnitude > float.Epsilon)
        {
            //MaxVelocity = collision.relativeVelocity.magnitude;
            //ActionPosition = collision.contacts[0].point;
            //ActionVector = collision.relativeVelocity;
            Vector2 old = lastSpeed;
            lastSpeed = rigid.velocity;
            if (old.sqrMagnitude <= float.Epsilon) return;
            Vector2 change = old - rigid.velocity;
            if (Vector2.Dot(change, old) < 0)
            {
                line = GetComponent<DrawLine>();
                line.Clear();
                line.Draw(rigid.position, -change);
                //float value = change.magnitude; 
                //if (CollisionPower < value) CollisionPower = value;
                //CollisionPower += value;
                //Debug.Log();
            }
            
        }
    }*/

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.relativeVelocity.magnitude > MaxVelocity)
        {
            MaxVelocity = collision.relativeVelocity.magnitude;
            //ActionPosition = collision.contacts[0].point;
            //ActionVector = collision.relativeVelocity;
            ActionPosition = rigid.transform.position;
            ActionVector = rigid.velocity;
            line = GetComponent<DrawLine>();
            line.Clear();
            line.Draw(ActionPosition, ActionVector);
        }
    }

    void OnGUI()
    {
        if (MaxVelocity > 0)
        {
            //DrawLine(ActionPosition, ActionPosition + ActionVector, 2);
            
        }
    }

    private void DrawLine(Vector2 start, Vector2 end, int width)
    {
        Vector2 d = end - start;
        float a = Mathf.Rad2Deg * Mathf.Atan(d.y / d.x);
        if (d.x < 0)
            a += 180;

        int width2 = (int)Mathf.Ceil(width / 2);

        GUIUtility.RotateAroundPivot(a, start);
        GUI.DrawTexture(new Rect(start.x, start.y - width2, d.magnitude, width), point);
        GUIUtility.RotateAroundPivot(-a, start);
    }
}
