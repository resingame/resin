﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class RestartScene : MonoBehaviour
{
    public KeyCode KeyRestarter = KeyCode.None;
	// Update is called once per frame
	void Update () {
        if (KeyRestarter != KeyCode.None && Input.GetKeyDown(KeyRestarter)) SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
