﻿using UnityEngine;
using System.Collections;

public class TestDamageTotal : MonoBehaviour
{
    private TestDamage[] damages = new TestDamage[0];
    public float MaxVelocity = 0f;
    public float CurrentMaxVelocity = 0f;
    private GishState state;
    // Use this for initialization
    void Start ()
    {
        damages = GetComponentsInChildren<TestDamage>();
        state = GetComponentInChildren<GishState>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
	    foreach (var damage in damages)
	    {
	        if (damage.MaxVelocity > MaxVelocity) MaxVelocity = damage.MaxVelocity;
	    }
	    if (state != null && state.CollisionPower > MaxVelocity) MaxVelocity = state.CollisionPower;

	}
}
