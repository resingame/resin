﻿using UnityEngine;
using System.Collections;
using KingDOM;
using UnityEngine.UI;

public class DamageShow : MonoBehaviour
{

    public Text text;
    [ComponentSelect]
    public Component source;

    [PropertyPopup(PropertyPopupAttribute.SourceType.FieldTarget, "source")] public string sourceField;
    // Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if (source != null && text != null && !string.IsNullOrEmpty(sourceField))
	    {
	        text.text = KingUtil.GetPropertyValue(source, sourceField).ToString();
	        /* if (source is TestDamage)
            {
                text.text = (source as TestDamage).MaxVelocity.ToString();
            }
            if (source is TestDamageTotal)
            {
                text.text = (source as TestDamageTotal).MaxVelocity.ToString();
            }*/

	    }
    }
}
