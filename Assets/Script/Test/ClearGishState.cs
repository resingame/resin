﻿using UnityEngine;
using System.Collections;

public class ClearGishState : MonoBehaviour
{
    private GishState state = null;
    // Use this for initialization
	void Start ()
	{
	    state = GetComponent<GishState>();
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.anyKeyDown)
	    {
	        state.Clear();
	        state.maxSpeedValue = 0;
	        state.maxChange = 0;
	        state.middleChange = 0f;
	    }
	}
}
