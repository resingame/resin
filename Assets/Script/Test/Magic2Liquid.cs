﻿using System;
using UnityEngine;
using System.Collections;

public class Magic2Liquid : MonoBehaviour
{
    public float ExplosionStrenght = -30f;
    public int ParticleSystemImIn = 0;

    public LPFixture ActiveFixture;
    public LPParticleSystem sys;
    public LPParticleMaterial fire;

    private LPManager lpman;
    private IntPtr shape;
    private IntPtr sysPtr;

    void Start()
    {
        lpman = FindObjectOfType<LPManager>();
        shape = ActiveFixture.GetShape();
        sysPtr = sys.GetPtr();
    }
    // Update is called once per frame
    void Update ()
    {
        Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 pos2 = new Vector2(pos.x, pos.y);
        if (Input.GetMouseButtonDown(0))
	    {
	        ChangeMaterial(pos2);    
	    }
	}

    void ChangeMaterial(Vector2 pos)
    {
        IntPtr particlesPtr = LPAPIParticleSystems.GetParticlesInShape(sysPtr, shape, pos.x, pos.y, 0);
        int[] particles = LiquidUtility.GetArray(particlesPtr);
        LPAPIParticles.SetSelectedParticleFlags(sys.GetPtr(), particles, fire.GetInt());
        LPAPIParticles.SetSelectedParticleColor(sys.GetPtr(), particles, 255, 0, 0, 255);
        LPAPIParticles.SetSelectedParticleUserData(sys.GetPtr(), particles, 1);
    }
}
