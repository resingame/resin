﻿using UnityEngine;
using System.Collections;
using KingDOM;
using KingDOM.Event;

public class LevelManager : MonoBehaviour
{

    
    public float TileSize = 1f;
    public CameraController CameraController = null;

    private bool isOk = false;
    private Match match;

    public Match Match
    {
        get { return match;}
    }

    // Use this for initialization
    void Awake ()
	{
	    isOk = true;
        isOk = isOk && (TileSize > 0) || isOk && KingUtil.CheckFailed("Не задан размер тайла");
        isOk = KingUtil.CheckNotNull(CameraController, "Не задан контроллер камеры.", isOk, this);
        if (!isOk) return;
        Sender.AddEvent(EventName.MAP_CREATE_START, GenerateLevel);
    }

    void OnDestroy()
    {
        Sender.RemoveEvent(EventName.MAP_CREATE_START, GenerateLevel);
    }

    private void GenerateLevel(SimpleEvent evnt)
    {
        if (evnt == null) return;
        if (!evnt.ExistParm<Match>(ParmName.MATCH))
        {
            KingUtil.CheckFailed("Уровень не может быть создан, не переданы настройки поединка.", this);
            return;
        }
        match = evnt.GetParm<Match>(ParmName.MATCH);
        match.scheme = new LevelScheme();
        if (!match.scheme.Init(match.SizeMap)) return;
        GenerateMapWithTemplate();
        GeneratePortal();
        SetCommands();
        GenerateMapFromTemplate();
        CameraController.Resize(match.SizeMap, TileSize);
        //WeaponBase.TileSize = TileSize;
        SM_Weapon.TileSize = TileSize;
        Sender.SendEvent(EventName.MAP_CREATE_END, this);
    }


    void SetCommands()
    {
        foreach (Team team in match.teams)
        {
            while (team.members.Count < match.TeamSize)
            {
                TeamMember member = team.Add(string.Format("Player {0}", team.members.Count)); 
                setMemberOnMap(member);
            }
        }
        
    }

    void setMemberOnMap(TeamMember member)
    {
        int x, y;
        do
        {
            x = randomTile();
            y = randomTile();
        } while ( x == 0 || x == match.SizeMap - 1 || y == 0 || y == match.SizeMap - 1 || match.scheme.map[x, y] == null);

        match.scheme.map[x, y] = null;
        member.gameObject.transform.parent = transform;
        member.gameObject.transform.localPosition = Idxs2Point(x, y);
    }

    void GenerateMapWithTemplate()
    {
        match.scheme.Generate(match.TilesSource);

    }

    void GenerateMapFromTemplate()
    {
        for (int i = 0; i < match.SizeMap; i++)
        {
            for (int j = 0; j < match.SizeMap; j++)
            {
                GameObject obj = match.scheme.map[i, j];
                if (obj == null) continue;
                GameObject tile = GameObject.Instantiate(obj);
                tile.transform.parent = transform;
                tile.transform.position = Idxs2Point(i, j);
                match.scheme.map[i, j] = obj;
            }
        }

    }

    void GeneratePortal()
    {
        Teleport t1 = CreatePortal(Idxs2Point(-4, -2), Idxs2Point(match.SizeMap + 3, -2), "Teleport 1");
        Teleport t2 = CreatePortal(Idxs2Point(-4, match.SizeMap + 1), Idxs2Point(match.SizeMap + 3, match.SizeMap + 1), "Teleport 2");
        t1.nextPoint = t2;
        t2.nextPoint = t1;
        Teleport t3 = CreatePortal(Idxs2Point(-2, -4), Idxs2Point(-2, match.SizeMap + 3), "Teleport 3");
        Teleport t4 = CreatePortal(Idxs2Point(match.SizeMap + 1, -4), Idxs2Point(match.SizeMap + 1, match.SizeMap + 3), "Teleport 4");
        t3.nextPoint = t4;
        t4.nextPoint = t3;
    }

    Teleport CreatePortal(Vector3 v1, Vector3 v2, string name)
    {
        Vector3 dis = v2 - v1;
        Teleport ret;
        GameObject go = new GameObject(name);
        go.transform.parent = this.transform;
        go.transform.localPosition = (dis)/2 + v1;
        BoxCollider2D collider = go.AddComponent<BoxCollider2D>();
        collider.isTrigger = true;
        collider.size = new Vector2(Mathf.Max(dis.x, TileSize), Mathf.Max(dis.y, TileSize) );
        return go.AddComponent<Teleport>();
    }

    public Vector3 Idxs2Point(int x, int y)
    {
        x = x - match.SizeMap /2;
        y = y - match.SizeMap /2;
        
        return new Vector3(x * TileSize, y * TileSize, 0);
    }

    int randomTile()
    {
        return Mathf.RoundToInt(Random.value*(match.SizeMap - 1));
    }
}
