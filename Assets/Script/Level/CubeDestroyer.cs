﻿using UnityEngine;
using System.Collections;

public class CubeDestroyer : MonoBehaviour {

    private LPManager lpman;
    private LPBody bod;
    // Use this for initialization
    void Start () {
        bod = GetComponent<LPBody>();
        lpman = FindObjectOfType<LPManager>();
        if (bod != null && lpman != null) bod.Initialise(lpman);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnDestroy()
    {
        if (bod != null && lpman != null)
            lpman.allBodies[bod.myIndex].Delete();
        Destroy(gameObject);
    }
}
