﻿using KingDOM;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class LevelScheme
{

    private int sizeSquare = 10;
    public Dictionary<TileModel, int> scheme = new Dictionary<TileModel, int>();
    public GameObject[,] map = new GameObject[0, 0];
    // Use this for initialization
    public bool Init(int size)
    {
        if (size <= 0)
        {
            return KingUtil.CheckFailed("LevelSchemeРазмер схемы лабиринта должен быть больше 0.");
        }
        sizeSquare = size;
        map = new GameObject[size, size];
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                map[i, j] = null;
            }
        }

        return true;
    }

    public void Generate(TileModel[] TilesSource)
    {
        GenerateRanged(TilesSource);
        GenerateUnlimited(TilesSource);

    }

    private void GenerateRanged(TileModel[] TilesSource)
    {
        var tiles = TilesSource.Where(p => p.model == TileModel.InsertModel.Range);
        if (tiles == null || tiles.Count() <= 0) return;
        foreach (TileModel tileModel in tiles)
        {
            scheme.Add(tileModel, Mathf.RoundToInt(tileModel.range.Random()));
        }
        var orederedscheme = scheme.OrderBy(v => v.Value);
        foreach (KeyValuePair<TileModel, int> pair in orederedscheme)
        {
            for (int i = 0; i < pair.Value; i++)
            {
                MapAddTile(pair.Key.tile, Mathf.RoundToInt(Random.value * (sizeSquare - 1)), Mathf.RoundToInt(Random.value * (sizeSquare - 1)));
            }
        }
    }

    private void GenerateUnlimited(TileModel[] TilesSource)
    {
        var tiles = TilesSource.Where(p => p.model == TileModel.InsertModel.Unlimited);
        if (tiles == null || tiles.Count() <= 0) return;
        RangeValue range = new RangeValue(0, tiles.Count() - 1);
        for (int i = 0; i < sizeSquare; i++)
        {
            for (int j = 0; j < sizeSquare; j++)
            {
                if (map[i, j] == null)
                {
                    int idx = Mathf.RoundToInt(range.Random());
                    GameObject obj = tiles.ElementAt(idx).tile;
                    map[i, j] = obj;
                }
            }
        }
    }

    void MapAddTile(GameObject tile, int x, int y)
    {
        int sx = x;
        int sy = y; 
        while (map[x, y] != null)
        {
            x++;
            if (x >= sizeSquare)
            {
                x = 0;
                y++;
                if (y >= sizeSquare) y = 0;
            }
            if (sx == x && sy == y) return;
        }
        map[x, y] = tile;
    }
}
