﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class CompareTileCounter : IComparer<KeyValuePair<TileModel, int> >
{
    public int Compare(KeyValuePair<TileModel, int> x, KeyValuePair<TileModel, int> y)
    {
        if (x.Value > y.Value) return 1;
        if (x.Value < y.Value) return -1;
        return 0;
    }
}
