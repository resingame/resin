﻿using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class BonusModel : ICloneable {

    public GameObject tile = null;
    public RangeValue range = RangeValue.one;
    public float probabilityOccurance = 0.01f;
    public float CheckDiameterEmpty = 1f;
    public float CheckDiameterConflict = 1f;
    public LayerMask maskEmpty = -1;
    public LayerMask maskConflict = -1;


    public object Clone()
    {
        BonusModel model = new BonusModel();
        model.range = range;
        model.tile = tile;
        model.probabilityOccurance = probabilityOccurance;
        model.CheckDiameterConflict = CheckDiameterConflict;
        model.CheckDiameterEmpty = CheckDiameterEmpty;
        model.maskEmpty = maskEmpty;
        model.maskConflict = maskConflict;
        return model;
    }
}
