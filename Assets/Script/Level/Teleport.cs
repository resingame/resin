﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using KingDOM;

[RequireComponent(typeof(Collider2D))]
public class Teleport : MonoBehaviour
{

    public Teleport nextPoint = null;
    private List<Collider2D> ignoreList = new List<Collider2D>();
    private Collider2D collider;
    private bool isOk = false;
    // Use this for initialization
	void Start ()
	{
	    isOk = KingUtil.CheckNotNull(nextPoint, "Не задана следующая точка для перемещения", true, this);
	    collider = GetComponent<Collider2D>();
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (ignoreList.Contains(other)) return;
        Vector3 point = transform.InverseTransformPoint(other.transform.position);
        ignoreList.Remove(other);
        nextPoint.Teporting(other, point);
    }

    void OnTriggerExit2D(Collider2D other)
    {
        ignoreList.Remove(other);
    }

    public void Teporting(Collider2D other, Vector3 point)
    {
        ignoreList.Add(other);
        other.transform.position = transform.TransformPoint(point);
    }

}
