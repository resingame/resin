﻿using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class TileModel:ICloneable {

    public enum InsertModel
    {
        Range,
        Unlimited
    }

    public GameObject tile = null;
    public InsertModel model = InsertModel.Range;
    public RangeValue range = RangeValue.one;



    public object Clone()
    {
        TileModel model = new TileModel();
        model.range = range;
        model.model = this.model;
        model.tile = GameObject.Instantiate(tile);
        return model;
    }
}
