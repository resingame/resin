﻿using UnityEngine;
using System.Collections;

public class MetaballCameraController : MonoBehaviour
{
    private RenderTexture metaTextures;
    public Material metaMaterial;

    private Camera cam;

    void Awake()
    {
        cam = GetComponent<Camera>();
    }

    public void Generate(Camera cam)
    {
        if (metaTextures != null) RenderTexture.ReleaseTemporary(metaTextures);
        metaTextures = RenderTexture.GetTemporary(cam.pixelWidth, cam.pixelHeight);
        //metaTextures = RenderTexture.GetTemporary(Screen.width, Screen.height);
        this.cam.targetTexture = metaTextures;
        metaMaterial.mainTexture = metaTextures;
        //Debug.Log(string.Format("{0}:{1} - {2}:{3}", cam.pixelWidth, cam.pixelHeight, Screen.width, Screen.height));
    }

    void OnDestroy()
    {
        if (metaTextures != null) RenderTexture.ReleaseTemporary(metaTextures);
    }
}
