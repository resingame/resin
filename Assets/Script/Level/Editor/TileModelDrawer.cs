﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomPropertyDrawer(typeof(TileModel))]
public class TileModelDrawer : PropertyDrawer {
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializedProperty propName = property.FindPropertyRelative("tile");
        EditorGUI.BeginProperty(position, label, property);

        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
        // Don't make child fields be indented
        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // Calculate rects
        float heightRow = EditorGUIUtility.singleLineHeight;
        Rect objectRect = new Rect(position.x, position.y, position.width, heightRow);
        Rect modelRect = new Rect(position.x, position.y + heightRow+1, 65, heightRow);
        Rect rangeRect = new Rect(position.x + 70, position.y + heightRow+1, 80, heightRow);

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        EditorGUI.PropertyField(objectRect, propName, GUIContent.none);
        SerializedProperty p = property.FindPropertyRelative("model");
        EditorGUI.PropertyField(modelRect, p, GUIContent.none);
        if (p.enumValueIndex == ((int) TileModel.InsertModel.Range))
        {
            EditorGUI.PropertyField(rangeRect, property.FindPropertyRelative("range"), GUIContent.none);
        }

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUIUtility.singleLineHeight * 2 + 3;
    }
}
