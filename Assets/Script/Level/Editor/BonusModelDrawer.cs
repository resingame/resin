﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomPropertyDrawer(typeof(BonusModel))]
public class BonusModelDrawer : PropertyDrawer
{

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializedProperty propName = property.FindPropertyRelative("tile");
        EditorGUI.BeginProperty(position, label, property);

        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
        // Don't make child fields be indented
        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // Calculate rects
        float heightRow = EditorGUIUtility.singleLineHeight;
        Rect objectRect = new Rect(position.x, position.y, 85, heightRow);
        Rect rangeRect = new Rect(position.x + 90, position.y, 80, heightRow);
        Rect rangeOccurance = new Rect(position.x + 250, position.y, 50, heightRow);

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        EditorGUI.PropertyField(objectRect, propName, GUIContent.none);
        EditorGUI.PropertyField(rangeRect, property.FindPropertyRelative("range"), GUIContent.none);
        EditorGUI.PropertyField(rangeOccurance, property.FindPropertyRelative("probabilityOccurance"), GUIContent.none);

        position.y += heightRow + 1;
        Rect rect = new Rect(position.x, position.y, 50, heightRow);
        EditorGUI.PropertyField(rect, property.FindPropertyRelative("CheckDiameterEmpty"), GUIContent.none);
        rect = new Rect(position.x + 60, position.y, 70, heightRow);
        EditorGUI.PropertyField(rect, property.FindPropertyRelative("maskEmpty"), GUIContent.none);
        rect = new Rect(position.x + 150, position.y, 50, heightRow);
        EditorGUI.PropertyField(rect, property.FindPropertyRelative("CheckDiameterConflict"), GUIContent.none);
        rect = new Rect(position.x + 210, position.y, 70, heightRow);
        EditorGUI.PropertyField(rect, property.FindPropertyRelative("maskConflict"), GUIContent.none);

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUIUtility.singleLineHeight * 2 + 3;
    }
}
