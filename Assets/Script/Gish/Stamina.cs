﻿using UnityEngine;
using System.Collections;
using KingDOM.Event;

public class Stamina : MonoBehaviour
{
    public int startEnergy = 1;
    protected int _energy = 1;
    public int HitCounter = 1;

    virtual public int Energy
    {
        get { return _energy; }
        set
        {
            float diff = value - _energy;
            _energy = value;

            if (_energy <= 0)
            {
                _energy = 0;
                Destroy(gameObject);
            }
        }
    }
    virtual public void Contact()
    {
        HitCounter--;

        if (HitCounter <= 0)
        {
            _energy = 0;
            Destroy(gameObject);
        }
    }

    void Start()
    {
        _energy = startEnergy;
    }
}
