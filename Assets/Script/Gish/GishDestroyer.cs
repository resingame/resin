﻿using System;
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using Random = UnityEngine.Random;

public class GishDestroyer : MonoBehaviour
{

    public GishControllerNew gish = null;
    public float ExplosionStrenght = 30f;
    public int ParticleSystemImIn = 0;

    public LPFixture ExplosionShape;
    public int StregthLiquid = 2;
    public float LifeTime = 10f;
    public LPFixture EyeFake = null;
    public LayerMask layer;
    public float ScaleStrength = 1f;

    private LPManager lpman;
    private IntPtr shape;

    private bool isOk = false;
    private Vector3[] Points;
    private GishState state = null;
    // Use this for initialization
	void Start ()
	{
	    if (gish == null) gish = GetComponent<GishControllerNew>();
	    isOk = gish != null;
        lpman = LiquidManager.LpManager;
        shape = ExplosionShape.GetShape();
	    state = GetComponent<GishState>();
	}

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Delete))
            DestroyImmediate(gish.gameObject);
    }

    void OnDestroy()
    {
        if (!isOk) return;
        float time = LifeTime;
        float timestep = LifeTime/StregthLiquid;
        for (int i = 0; i < StregthLiquid; i++)
        {
            MakePolyPart();
            LifeTime -= timestep;
        }
        
        //MakeMine();
        LifeTime = time;
    }

    void MakePolyPart()
    {
        GameObject PolyPart = new GameObject("GishLiquid");
        PolyPart.layer = layer.value;
        //PolyPart.transform.position = gish.transform.position;
        //Add an LPParticleGroupPoly to the gameobject 
        LPParticleGroupPoly group = PolyPart.AddComponent<LPParticleGroupPoly>();
        group.LifeTime = LifeTime;

        Points = new Vector3[state.rigids.Length];
        for (int i = 0; i < Points.Length; i++)
        {
            Points[i] = state.rigids[i].transform.position;
        }

        //Call DefinePoints to set the polys points programmatically
        group.DefinePoints(Points);
        if (state != null) group.LinearVelocity = state.maxSpeed * ScaleStrength;

        //Call initialise on the LPParticleGroupPoly component passing in the chosen LPParticleSystem component	
        //In this case it must be particlesystem 0, as there is only one particlesystem in this scene	
        LPParticleSystem ps = FindObjectOfType<LPManager>().ParticleSystems[0];
        group.Initialise(ps); //*/
        foreach (LPParticle particle in ps.Particles)
        {
            particle.LifeTime = (0.5f + Random.value)*LifeTime/2;
        }
        
        //PolyPart.SetActive(false);
    }

    void MakeMine()
    {
        Transform transform = ExplosionShape.transform;
        IntPtr partsysptr = lpman.ParticleSystems[ParticleSystemImIn].GetPtr();
        IntPtr particlesPointer = LPAPIParticleSystems.GetParticlesInShape(partsysptr
                                                                           , shape, transform.position.x, transform.position.y
                                                                           , transform.rotation.eulerAngles.z);

        int[] particlesArray = new int[1];
        Marshal.Copy(particlesPointer, particlesArray, 0, 1);
        int foundNum = particlesArray[0];


        if (foundNum > 0)
        {
            int[] Indices = new int[foundNum + 1];
            Marshal.Copy(particlesPointer, Indices, 0, foundNum + 1);

            LPAPIParticles.ExplodeSelectedParticles(partsysptr, Indices, transform.position.x, transform.position.y, ExplosionStrenght);
        }
        if (EyeFake != null)
        {
            EyeFake.gameObject.SetActive(true);
            EyeFake.transform.position = ExplosionShape.transform.position;
            EyeFake.transform.rotation = ExplosionShape.transform.rotation;
            LPBody body = EyeFake.GetComponent<LPBody>();
            body.Initialise(lpman);
            LPAPIBody.ApplyLinearImpulseToBody(body.GetPtr(), (Random.value - 0.5f) * ExplosionStrenght, Random.value * ExplosionStrenght, EyeFake.transform.position.x, EyeFake.transform.position.y, true);
            EyeFake.Initialise(body);
        }
        /*LPBody b = ExplosionShape.GetComponent< LPBody>();
        if (b != null)
        {
            b.Initialise(lpman);
            //b.Delete();
        }*/
        Destroy(ExplosionShape.gameObject);
    }
}
