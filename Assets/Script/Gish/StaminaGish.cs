﻿using UnityEngine;
using System.Collections;
using KingDOM.Event;

public class StaminaGish : Stamina
{
    public override int Energy {
        get { return _energy; }
        set
        {
            float diff = value - _energy;
            _energy = value;
            if (diff < 0)
            {
                Sender.SendEvent(EventName.WARRIOR_GET_DAMAGE, this, ParmName.DAMAGE, diff);
            }
            else
            {
                Sender.SendEvent(EventName.WARRIOR_GET_HEALS, this, ParmName.DAMAGE, diff);
            }
            if (_energy <= 0)
            {
                _energy = 0;
                Sender.SendEvent(EventName.WARRIOR_DIE, this);
                Destroy(gameObject);
            }
        }
    }
}
