﻿using UnityEngine;
using System.Collections;

public class StaminaBlock : Stamina
{
    public int HitCounter = 1;
    public override int Energy {
        get { return _energy; }
        set
        {
            if (_energy > value) HitCounter--;
            _energy = value;

            if (HitCounter <= 0)
            {
                _energy = 0;
                Destroy(gameObject);
            }
        }
    }
}
