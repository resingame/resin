﻿using UnityEngine;
using System.Collections;

public class InputGishNew : MonoBehaviour
{

    public GishControllerNew gish = null;
    public float DeadZone = 0.3f;
    public float AxisValueLeft = 0f;
    public float AxisValueRight = 0f;

    //инициализация прошла успешно
    public bool isOk = false;
    private bool isWait = false;
    private bool shootExistLast = false;
    // Use this for initialization
    void Awake()
    {
        gish = GetComponent<GishControllerNew>();
        isOk = gish != null;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isOk) return;
        AxisValueLeft = Input.GetAxis("Left Trigger");
        AxisValueRight = Input.GetAxis("Right Trigger");
        bool catching = Input.GetAxis("Right Trigger") > DeadZone;
        bool sticking = Input.GetAxis("Left Trigger") > DeadZone;
        bool jumping = Input.GetButtonDown("Jump");
        //bool aiming = Input.GetButton("Fire");
        float shootPower = Input.GetAxis("Fire1");
        bool shootExist = Mathf.Abs(shootPower) > DeadZone;
        bool shootStart = Input.GetButtonDown("FireTest");
        bool shootEnd = Input.GetButtonUp("FireTest");
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        float xAim = Input.GetAxis("Aim X");
        float yAim = Input.GetAxis("Aim Y");

        shootStart = shootStart || (!shootExistLast && shootExist);
        shootEnd = shootEnd || (shootExistLast && !shootExist);
        if (shootStart) Debug.Log("Shoot start");
        if (shootExist) Debug.Log("Shoot");
        if (shootEnd) Debug.Log("Shoot end");
        shootExistLast = shootExist;


        if (gish.weapon != null)
        {
            Vector2 direction = Vector2.up * yAim + Vector2.right * xAim;
            gish.weapon.NewDirection = direction;
            gish.weapon.Position = gish.Center;
            gish.weapon.StartShot = shootStart;
            gish.weapon.EndShot = shootEnd;
            //gish.weapon.ShotPower = (shootReset && rightTriggerChange) ? rightTriggerAxis: 1f;

        }
        if (isWait)
        {
            if (catching || sticking || jumping || shootEnd || Mathf.Abs(x) > float.Epsilon || Mathf.Abs(y) > float.Epsilon || Mathf.Abs(xAim) > float.Epsilon || Mathf.Abs(yAim) > float.Epsilon)
            {
                isWait = false;
            }
            else
            {
                return;
            }
        }
        //gish.IsCatch = catching || aiming;
        gish.IsCatch = catching;

        //if (shooting && gish.weapon != null)
        //{
        //Vector2 direction = Vector2.up * y + Vector2.right * x;
        //gish.weapon.Shot(gish.Center, direction);
        //}
        //else if (gish.weapon != null)
        //{
        //    if (aiming)
        //    {
        //        Vector2 direction = Vector2.up * y + Vector2.right * x;
        //        if (direction.sqrMagnitude < float.Epsilon)
        //        {
        //            gish.weapon.DestroyAim();
        //        }
        //        else
        //        {
        //            gish.weapon.Aim(gish.Center, direction);
        //        }
        //    }
        //    else
        //    {
        //        gish.weapon.DestroyAim();
        //    }
        //}

        if (gish.IsCatch) return;

        gish.IsStick = sticking;

        gish.moveDirection = new Vector2(x, y);
        if (jumping) gish.IsJumped = jumping;



    }

    void OnEnable()
    {
        if (!isOk) return;
        gish.isActive = true;
        shootExistLast = Mathf.Abs(Input.GetAxis("Fire1")) > DeadZone;
    }

    void OnDisable()
    {
        if (!isOk) return;
        isWait = true;
        //gish.IsCatch = true;
        gish.moveDirection = Vector2.zero;
        gish.isActive = false;
        gish.IsStick = false;
        //gish.SetVelocity(Vector2.zero);
    }
}
