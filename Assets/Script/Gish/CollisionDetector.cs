﻿using UnityEngine;
using System.Collections;
using KingDOM;

public class CollisionDetector : MonoBehaviour
{
    private GishState state;
    private bool isOk = false;
    public Rigidbody2D rigid;
    // Use this for initialization
    void Start()
    {
        state = GetComponentInParent<GishState>();
        isOk = true;
        isOk = KingUtil.CheckNotNull(state, "Не найдена ссылка на состояние.", isOk, this);
        if (!isOk) enabled = false;
        rigid = GetComponent<Rigidbody2D>();
    }

    public void FixedUpdate()
    {
        float power = rigid.velocity.magnitude;
        if (power > 0)
        {
            //state.SetPower(power);
            state.SetVelocity(rigid.velocity);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {

        state.CollisionCount();
    }

    /*void OnCollisionEnter2D(Collision2D collision)
    {

        //float power = collision.relativeVelocity.magnitude;
        float power = rigid.velocity.magnitude;
        if (power > 0)
        {
            //state.SetPower(power);
            state.SetVelocity(rigid.velocity);
        }
    }
    void OnCollisionStay2D(Collision2D collision)
    {

        //float power = collision.relativeVelocity.magnitude;
        float power = rigid.velocity.magnitude;
        if (power > 0)
        {
            //state.SetPower(power);
            state.SetVelocity(rigid.velocity);
        }
    }*/
}
