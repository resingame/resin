﻿using System;
using UnityEngine;
using System.Collections;
using System.Reflection;

[Serializable]
public class Reaction {

    public enum Element
    {
        Fire,
        Light,
        Water,
        Mind,
        Pressure
    }
    [EnumLabel("Тип")]
    public enum Type
    {
        [EnumLabel(" ")]
        None,
        [EnumLabel("Контакт")]
        Contact,
        [EnumLabel("Повреждение")]
        Damage,
        [EnumLabel("Воздействие")]
        Modifier,
        [EnumLabel("Подмена")]
        Change
    }

    public Element source = Element.Fire;
    public Type action= Type.None;
    public float degreeEffect = 1.0f;
    public float whileMoreValue = 0f;
    public float whileLessValue = float.MaxValue;
    public Component modifier = null;
    public GameObject changeTo = null;

    public int impact(Stamina stamina, float power)
    {
        int newValue = stamina.Energy - Mathf.RoundToInt(power*degreeEffect);
        if (action == Type.Contact)
        {
            stamina.Contact();
        }
        else if (stamina.Energy > whileMoreValue && stamina.Energy <= whileLessValue ||
            newValue <= whileMoreValue && newValue <= whileLessValue)
        {
            switch (action)
            {
                case Type.Damage:
                    stamina.Energy = newValue;
                    break;
                case Type.Modifier:
                    CopyComponent(stamina.gameObject, modifier);
                    break;
                case Type.Change:
                    GameObject go =
                        GameObject.Instantiate(changeTo, stamina.transform.position, stamina.transform.rotation) as
                            GameObject;
                    go.name = stamina.gameObject.name;
                    go.transform.parent = stamina.gameObject.transform;
                    break;
            }
        }
        
        return newValue;
    }

    public void CopyComponent(GameObject target, Component source)
    {
        Component new_component = target.AddComponent(source.GetType());
        foreach (FieldInfo f in source.GetType().GetFields())
        {
            f.SetValue(new_component, f.GetValue(source));
        }
        foreach (PropertyInfo p in source.GetType().GetProperties())
        {
            p.SetValue(new_component, p.GetValue(source, null), null);
        }
    }
}
