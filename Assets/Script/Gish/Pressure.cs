﻿using UnityEngine;
using System.Collections;
using KingDOM;

public class Pressure : MonoBehaviour {

    public float SafetyCollisionPower;
    public float roundOff = 0.5f;
    public Behaviour behaviour;
    
    // Use this for initialization
    void Start ()
    {
        GishState state = GetComponentInChildren<GishState>();
        if (state != null)
        {
            state.collisionStart += collision;
        }
        if (behaviour == null)
        {
            behaviour = GetComponentInChildren<Behaviour>();
        }
    }
	
	void collision (float value)
	{
	    value = KingUtil.Round(value, roundOff);
        if (value > SafetyCollisionPower && behaviour != null)
	    {
	        behaviour.Effect(Reaction.Element.Pressure, value - SafetyCollisionPower);
	    }
	}
}
