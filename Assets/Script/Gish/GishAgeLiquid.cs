﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GishAgeLiquid : MonoBehaviour
{
    public LPParticleSystem ParticleSystem;
    [PropertyBase]
    [PropertyArgs(tip="Скорость исчезновения доли жидкости в секунду.")]
    public float DryingSpeed = 0.05f;

    private float lastTime = float.MaxValue; 
    // Use this for initialization
	void Awake ()
	{
	    if (ParticleSystem == null) ParticleSystem = GetComponent<LPParticleSystem>();
	}
	
	// Update is called once per frame
	void Update ()
	{
        if (lastTime > Time.time) return;
	    
        List<int> deletes = new List<int>();
        LPParticle[] allParticles = ParticleSystem.Particles;
        //LPAPIParticleSystems.GetParticlesInShape(P)
	    int cnt = Mathf.CeilToInt(DryingSpeed * ((int)(Time.time - lastTime)));
	    if (cnt > 0)
	    {
	        for (int i = 0; i < cnt; i++)
	        {
	            //deletes.Add(allParticles[Random.Range(0, cnt - 1)]);
	        }
	    }
	}
}
