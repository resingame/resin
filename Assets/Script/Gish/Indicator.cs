﻿using UnityEngine;
using System.Collections;
using KingDOM;
using UnityEngine.UI;

public class Indicator : MonoBehaviour
{
    public Text TxtName = null;
    public Text TxtResilience = null;
    public SpriteRenderer imageCurrent = null;
    public bool AutoInit = false;
    private TeamMember member;
    private Stamina _stamina;
    private InputGishNew input;
    private Color color;
    private bool isOk = false;

    // Use this for initialization
	void Awake ()
	{
        color = Color.cyan;
        member = GetComponentInChildren<TeamMember>();
	    _stamina = GetComponentInChildren<Stamina>();
	    input = GetComponentInChildren<InputGishNew>();
	    isOk = true;
        isOk = KingUtil.CheckNotNull(TxtName, "Нет ссылки на поле вывода имени.", isOk, this);
        isOk = KingUtil.CheckNotNull(TxtResilience, "Нет ссылки на поле вывода силы.", isOk, this);
        isOk = KingUtil.CheckNotNull(imageCurrent, "Нет ссылки на индикатор текущего игрока.", isOk, this);
        isOk = KingUtil.CheckNotNull(member, "Нет ссылки на данные о команде.", isOk, this);
        isOk = KingUtil.CheckNotNull(_stamina, "Нет ссылки на вязкость.", isOk, this);
        isOk = KingUtil.CheckNotNull(input, "Нет ссылки на контроллер управления гишем.", isOk, this);
        if (isOk) Init(color);
    }

    // Update is called once per frame
    void Update () {
	    if (!isOk) return;
        TxtResilience.text = _stamina.Energy.ToString();
        if (imageCurrent != null)
            imageCurrent.enabled = input != null ? input.enabled : false;
    }

    public void Init(Color color)
    {
        if (!isOk) return;
        this.color = color;
        TxtName.text = member.name;
        TxtName.color = updateColor(TxtName.color);
        TxtResilience.color = updateColor(TxtResilience.color);
        imageCurrent.color = updateColor(imageCurrent.color);
    }

    private Color updateColor(Color fromColor)
    {
        return new Color(color.r, color.g, color.b, fromColor.a);
    }
}
