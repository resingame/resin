﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Stamina))]
public class Behaviour : MonoBehaviour
{
    public Reaction[] Reactions = null;
    private Stamina _stamina;

    void Awake()
    {
        _stamina = GetComponent<Stamina>();
    }

    public void Effect(Reaction.Element source, float power)
    {
        foreach (Reaction reaction in Reactions)
        {
            if (reaction.source == source)
            {
                reaction.impact(_stamina,power);
            }
        }
    }
}
