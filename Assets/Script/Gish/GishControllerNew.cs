﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using KingDOM.Event;

public class GishControllerNew : MonoBehaviour
{
    public bool isActive = false;
    public bool InertiaWorkFlag = false;
    public Vector2 moveDirection = Vector2.zero;
    //ограничение скорости
    public float VelocityLimit = 3;
    public bool IsStick = false;
    private bool _isCatch = false;
    public bool IsJumped = false;
    //сила на перемещение по горизонтали
    public float forceMove = 3;
    //сила прилипания к поверхности
    public float forceStick = 7;
    //сила прилипания к поверхности
    public float forceJump = 7;
    //жесткость пружины при прилипании
    private float inflexibility = 1;
    public float selectSpringsMore = 0.5f;
    // твердые тела
    public SpringJoint2D[] springs = null;
    private Collider2D[] colliders = null;
    private FixedJoint2D[] joints = null;
    // центральный объект
    public Transform CentralTransform = null;

    // маска слоев от которых капля может отталкиваться
    public LayerMask MoveMask = Physics2D.AllLayers;
    //Маска слоев к которым капля может прилипать
    public LayerMask StickMask = Physics2D.AllLayers;

    // Дистанция для проверки соприкосновения с поверхностью
    public float checkDistance = 0.1f;
    // минимальная сила ниже которой воздействие будет игнорироваться при движении
    public float checkForce = 0.1f;

    [ComponentSelect(typeof(SM_Weapon))]
    public SM_Weapon weapon = null;

    public float TimeForStop;
    private float rateStop;

    public float CheckFallenHorizontal = 0.1f;

    public bool IsLastMove = false;
    public float CheckRadius = 0.5f;

    private GishState state = null;
    public bool TestWeapon = false;
    private bool previousUpdateHasContact = false;
    private Vector2 CheckDisplace = Vector2.zero;
    private Vector2 tangent = Vector2.zero;
    private Vector2 normal = Vector2.zero;


    //Центр гиша
    public Vector3 Center
    {
        get
        {
            Vector3 center;
            // если задан центральный объект (например глаз) центр считается относительно него
            if (CentralTransform != null)
            {
                center = CentralTransform.transform.position;
            }
            else // в противном случае  высчитываем центр между всеми центрами rigidbody
            {
                center = state.Center;

            }
            return center;
        }
    }

    public bool IsCatch
    {
        get { return _isCatch; }
        set
        {
            if (_isCatch == value) return;
            _isCatch = value;
            Catching(value);
        }
    }

    void Catching(bool use)
    {
        if (!use || joints != null && joints.Length > 0)
        {
            foreach (FixedJoint2D joint in joints)
            {
                DestroyImmediate(joint);
            }
        }
        if (use)
        {
            Vector2 center = Center;
            List<FixedJoint2D> jj = new List<FixedJoint2D>();
            for (int i = 0; i < state.rigids.Length; i++)
            {

                Rigidbody2D rigid = state.rigids[i];
                Vector2 dir = (center - rigid.worldCenterOfMass).normalized;
                RaycastHit2D hit = Physics2D.Raycast(rigid.worldCenterOfMass, -dir, checkDistance, MoveMask.value);
                if (hit.collider != null)
                {
                    FixedJoint2D j = rigid.gameObject.AddComponent<FixedJoint2D>();
                    j.connectedBody = hit.rigidbody;
                    j.connectedAnchor = hit.point;
                    jj.Add(j);
                }
            }

            joints = jj.ToArray();
            if (joints.Length > 0)
            {
                SetVelocity(Vector2.zero);
            }
        }

    }

    void Awake()
    {

        if (!TestWeapon) weapon = null;
        state = GetComponentInParent<GishState>();
        state.SetController(this);
        colliders = new Collider2D[state.rigids.Length];
        rateStop = TimeForStop > float.Epsilon ? Time.fixedDeltaTime / TimeForStop: 1;
        for (int i = 0; i < state.rigids.Length; i++)
        {
            colliders[i] = state.rigids[i].GetComponent<Collider2D>();
        }
        if (springs == null || springs.Length <= 0)
        {
            InitSprings();
        }
    }

    void FixedUpdate()
    {
        if (CentralTransform != null)
        {
            CentralTransform.position = state.Center;
        }
        bool notFreeFall = !state.isMoving;
        bool hasContact = isTouch(MoveMask);
        normal = GetNormal(MoveMask);
        //bool hasContact = isTouch(MoveMask);
        // расчет прилипания если не нажата клавиша прыжок, иначе мы отцепляемся
        if (IsStick && !IsJumped)
        {
            //определяем направление в котором мы должны прилипать
            Vector2 dirStick = CalcStickForce();
            //прикладываем усилие в отношении всей капли
            AddForce(dirStick * forceStick);
            //нейтрализуем гравитацию для объекта
            if (hasContact) AddForce(-Physics2D.gravity / Time.fixedDeltaTime);
        }


        Vector2 speed = state.Velocity;
        // горизонтальное смещение
        //float x = notFreeFall ? moveDirection.x * forceMove : 0;
        float x = hasContact ? moveDirection.x * forceMove : 0;
        // вертикальное смещение
        //float y = IsStick && notFreeFall ? moveDirection.y * forceMove : 0;
        float y = IsStick && hasContact ? moveDirection.y * forceMove : 0;
        Vector2 f = new Vector2(x, y);
        tangent = GetTangent(normal, f);
        //расчитываем движение по клавишам
        Vector2 dirMove = CalcMoveForce();

        float m = dirMove.magnitude;
        if (m != 0)
        {
            //f = Vector2.Dot(f, dirMove) / m * dirMove;
            Vector2 v = new Vector2(f.x, 0);
            Vector2 d = new Vector2(dirMove.x, 0);
            float checkVal = Vector2.Dot(v, d);
            float xMove = checkVal > 0 ? v.x : 0;
            v = new Vector2(0, f.y);
            d = new Vector2(0, dirMove.y);
            checkVal = Vector2.Dot(v, d);
            float yMove = checkVal > 0 ? v.y : 0;
            f = new Vector2(xMove, yMove);
        }
        var fmagn = f.magnitude;
        /*Vector2 displace = moveDirection * checkDistance;
        CheckDisplace = displace;
        //проверяем что туда куда мы хотим сдвинуть гиша нет предмета
        bool gishInWall = Physics2D.CircleCast(state.Center, CheckRadius, displace.normalized, displace.magnitude, MoveMask);
        if (gishInWall && hasContact && !IsStick && fmagn >= float.Epsilon)
        {
            AddForceMove(displace);
            IsLastMove = false;
        }
        else*/
        {
            AddForce(f);
        }
        IsLastMove = fmagn > float.Epsilon;
        //if (!state.isMoving && f.sqrMagnitude > float.Epsilon)
        if ((notFreeFall || previousUpdateHasContact) && f.sqrMagnitude > float.Epsilon)
            RecalcVelocity(f.normalized);

        Vector2 speedTangent = GetTangent(normal, speed);
        if (hasContact && fmagn < float.Epsilon && Mathf.Abs(speedTangent.x) > CheckFallenHorizontal && (isActive || InertiaWorkFlag)) ReduceInertia();
        //расчет прыжка
        if (IsJumped)
        {
            Vector2 dirJump = CalcJumpForce();
            AddForce(dirJump * forceJump, ForceMode2D.Impulse);
            IsJumped = false;
        }
        previousUpdateHasContact = hasContact;
    }

    void OnDestroy()
    {
        Sender.SendEvent(EventName.WARRIOR_DIE, this);
        Destroy(this.weapon);
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        CheckDisplace.Normalize();
        Gizmos.DrawWireSphere(Center + new Vector3(CheckDisplace.x, CheckDisplace.y), CheckRadius);
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(Center, Center + new Vector3(CheckDisplace.x, CheckDisplace.y)*5);
    }
    /// <summary>
    /// Снизить инерцию
    /// </summary>
    void ReduceInertia()
    {
        float maxVel = 0f;
        foreach (Rigidbody2D rigid in state.rigids)
        {
            rigid.velocity = rigid.velocity * (1 - rateStop);
            maxVel = Mathf.Max(maxVel, rigid.velocity.sqrMagnitude);
        }
        if (maxVel <= float.Epsilon) InertiaWorkFlag = false;
    }

    public void SetNewWeapon(GameObject weapon)
    {
        GameObject old = this.weapon != null? this.weapon.gameObject: null;
        GameObject newWeapon = Instantiate(weapon);
        newWeapon.transform.position = transform.position;
        this.weapon = newWeapon.GetComponent<SM_Weapon>();
        if (old != null) this.weapon.NewDirection = old.GetComponent<SM_Weapon>().LastDirection;
        this.weapon.Reload();
        newWeapon.transform.parent = transform.parent;
        SM_Weapon.OWNER = GetComponentInParent<Behaviour>();
        if (old != null)
            DestroyImmediate(old.gameObject);
    }

    void RecalcVelocity(Vector2 moveForce)
    {
        if ((IsStick && isTouch(StickMask) || (!IsStick && isTouch(MoveMask))) && (moveForce.x != 0 || moveForce.y != 0))
        {
            if (Input.GetKey(KeyCode.RightControl))
                Debug.Log("Start check");
            foreach (Rigidbody2D rigid in state.rigids)
            {
                rigid.velocity = Vector2.ClampMagnitude(rigid.velocity, VelocityLimit);
            }

        }
    }

    void InitSprings()
    {
        SpringJoint2D[] springs = GetComponentsInChildren<SpringJoint2D>();
        List<SpringJoint2D> selects = new List<SpringJoint2D>();
        foreach (SpringJoint2D spring in springs)
        {
            if (spring.distance > selectSpringsMore)
            {
                spring.autoConfigureDistance = false;
                selects.Add(spring);
            }
        }
        this.springs = selects.ToArray();
    }

    /// <summary>
    /// Приложить силу к капле
    /// </summary>
    /// <param name="forceValue"> значение силы </param>
    /// <param name="forceMode"> режим приложения силы</param>
    /// <param name="mask">Маска слоев при соприкосновении с которыми срабатывает сила. Если маска не задана, то сила приклаывается ко всей капле </param>
    public void AddForce(Vector2 forceValue, ForceMode2D forceMode = ForceMode2D.Force, int mask = -1)
    {
        Rigidbody2D[] selectRigids = new Rigidbody2D[state.rigids.Length];
        if (mask != -1)
        {
            Vector2 center = Center;
            for (int i = 0; i < state.rigids.Length; i++)
            {

                Rigidbody2D rigid = state.rigids[i];
                Vector3 dir = (center - rigid.worldCenterOfMass).normalized;
                Collider2D collider = colliders[i];
                selectRigids[i] = (collider == null || !collider.IsTouchingLayers(mask)) ? null : rigid;

            }
        }
        else
        {
            selectRigids = state.rigids;
        }
        Vector2 val = forceValue*Time.fixedDeltaTime;

        foreach (Rigidbody2D rigid in selectRigids)
        {
			if (rigid != null)
				rigid.AddForce(val, forceMode);
        }
    }

	// Uses MovePosition() instead of AddForce() to disable ineartia
	public void AddForceMove(Vector2 dirMove, int mask = -1)
    {
        Rigidbody2D[] selectRigids = new Rigidbody2D[state.rigids.Length];
	    if (!IsStick) dirMove.y = 0f;
        if (mask != -1)
        {
            Vector2 center = Center;
            for (int i = 0; i < state.rigids.Length; i++)
            {

                Rigidbody2D rigid = state.rigids[i];
                Vector3 dir = (center - rigid.worldCenterOfMass).normalized;
                Collider2D collider = colliders[i];
                selectRigids[i] = (collider == null || !collider.IsTouchingLayers(mask)) ? null : rigid;

            }
        }
        else
        {
            selectRigids = state.rigids;
        }
			
        foreach (Rigidbody2D rigid in selectRigids)
        {
			if (rigid != null) 
				rigid.MovePosition(rigid.position + dirMove);
        }
    }
    /// <summary>
    /// Приложить силу на прилипание. Сила прикаывадется только к тем точкам которые имеют соприкосновение с поверхностью
    /// </summary>
    /// <param name="forceValue">значение силы</param>
    /// <param name="forceMode">режим приложения силы</param>
    public void AddForceStick(float forceValue, ForceMode2D forceMode = ForceMode2D.Force)
    {
        Vector2 center = Center;
        for (int i = 0; i < state.rigids.Length; i++)
        {

            Rigidbody2D rigid = state.rigids[i];
            Vector2 dir = (center - rigid.worldCenterOfMass).normalized;
            RaycastHit2D hit = Physics2D.Raycast(rigid.worldCenterOfMass, -dir, checkDistance, StickMask.value);
            if (hit.collider != null)
            {
                rigid.AddForce(-dir * forceValue, forceMode);
            }
        }

    }

    public void SetInFlexibility(float value)
    {
        foreach (SpringJoint2D spring in springs)
        {
            spring.frequency = spring.frequency / inflexibility * value;

        }
        inflexibility = value;
    }

    //Рассчитать направление силы  для прилипания при соприкосновении с поверхностью
    public Vector2 CalcStickForce()
    {

        Vector2 stickForce = Vector2.zero;
        Vector2 maskForce = GetMaskMoving();

        stickForce = -normal;
        stickForce.Scale(maskForce);
        stickForce = stickForce.normalized;//activeRigids > 0 ? stickForce.normalized / activeRigids: Vector2.zero;
        //Debug.DrawRay(center, stickForce, Color.magenta);
        return stickForce;

    }
    /*public Vector2 CalcStickForce()
    {

        Vector2 stickForce = Vector2.zero;
        int activeRigids = 0;
        Vector2 maskForce = GetMaskMoving();

        Vector2 center = Center;

        for (int i = 0; i < state.rigids.Length; i++)
        {

            Rigidbody2D rigid = state.rigids[i];
            Vector2 dir = (center - rigid.worldCenterOfMass).normalized;
            RaycastHit2D hit = Physics2D.Raycast(rigid.worldCenterOfMass, -dir, checkDistance, MoveMask.value);
            if (hit.collider != null)
            {
                activeRigids++;
                Vector2 n = hit.normal;
                n.Scale(maskForce);
                stickForce += -n;
            }
        }
        stickForce = stickForce.normalized;//activeRigids > 0 ? stickForce.normalized / activeRigids: Vector2.zero;
        Debug.DrawRay(center, stickForce, Color.magenta);
        return stickForce;

    }*/

    // рассчитать направление силы прыжка в зависимости от точек соприкосновения с поверхностью
    public Vector2 CalcJumpForce()
    {
       return normal;
    }

    /// <summary>
    /// рассчитать силу перемещения в пространстве исходя из точек соприкосновения с поверхностью
    /// </summary>
    /// <returns></returns>
    public Vector2 CalcMoveForce()
    {
        Vector2 moveForce = tangent;
        Vector2 maskForce = GetMaskMoving();
        moveForce = moveForce.normalized;// * activeRigids / rigids.Length;
        if (IsStick) moveForce.Scale(maskForce);
        return moveForce;
    }
    /*public Vector2 CalcMoveForce()
    {
        Vector2 moveForce = Vector2.zero;
        Vector2 center = Center;
        Vector2 maskForce = GetMaskMoving();
        int activeRigids = 0;
        for (int i = 0; i < state.rigids.Length; i++)
        {

            Rigidbody2D rigid = state.rigids[i];
            Vector2 dir = (center - rigid.worldCenterOfMass).normalized;
            RaycastHit2D hit = Physics2D.Raycast(rigid.worldCenterOfMass, -dir, checkDistance, MoveMask.value);
            if (hit.collider != null)
            {
                Vector2 n = hit.normal;
                if (IsStick) n.Scale(maskForce);
                float vert = n.x;
                float horz = -n.y;

                moveForce += Vector2.right * horz + Vector2.up * vert;
                Debug.DrawRay(rigid.worldCenterOfMass, hit.normal.normalized, Color.cyan);
                activeRigids++;
            }
        }
        moveForce = moveForce.normalized;// * activeRigids / rigids.Length;
        Debug.DrawRay(center, moveForce, Color.green);
        return moveForce;
    }*/

    public void MovePosition(Vector3 displace, int mask = -1)
    {
        foreach (Rigidbody2D rigid in state.rigids)
        {
            if (mask != -1 && !rigid.IsTouchingLayers(mask)) continue;

            rigid.MovePosition(rigid.transform.position + displace);
        }
    }

    public bool isTouch(int mask)
    {
        foreach (Rigidbody2D rigid in state.rigids)
        {
            if (rigid.IsTouchingLayers(mask)) return true;
        }
        return false;
    }

    public void SetVelocity(Vector2 limit, int mask = -1)
    {
        Vector2 speed = state.Velocity;
        speed.Scale(limit);
        Vector2 center = Center;
        for (int i = 0; i < state.rigids.Length; i++)
        {
            if (Input.GetKey(KeyCode.RightControl))
                Debug.Log("Start check");
            Rigidbody2D rigid = state.rigids[i];

            if (mask == -1)
            {
                rigid.velocity = Vector2.ClampMagnitude(speed, VelocityLimit);
            }
            else
            {
                Vector2 dir = (center - rigid.worldCenterOfMass).normalized;
                RaycastHit2D hit = Physics2D.Raycast(rigid.worldCenterOfMass, -dir, checkDistance, MoveMask.value);
                if (hit.collider != null)
                {
                    rigid.velocity = Vector2.ClampMagnitude(speed, VelocityLimit);

                }
            }
        }
    }

    private Vector2 GetNormal(int mask = -1)
    {
        Vector2 normal = Vector2.zero;
        Vector2 center = Center;
        RaycastHit2D hit;
        for (int i = 0; i < state.rigids.Length; i++)
        {

            Rigidbody2D rigid = state.rigids[i];
            Vector2 dir = (center - rigid.worldCenterOfMass).normalized;
            if (mask == -1)
            {
                hit = Physics2D.Raycast(rigid.worldCenterOfMass, -dir, checkDistance);
            }
            else
            {
                hit = Physics2D.Raycast(rigid.worldCenterOfMass, -dir, checkDistance, mask);
            }

            if (hit.collider != null)
            {
                normal += hit.normal;
            }
        }
        return normal.normalized;
    }

    private Vector2 GetTangent(Vector2 normal, Vector2 direction)
    {
        Vector3 cross = Vector3.Cross(-normal, direction);
        if (cross.sqrMagnitude <= float.Epsilon) return Vector2.zero;
        Vector2 tangent = Vector3.Cross(normal, cross);
        return tangent;
    }

    private Vector2 GetMaskMoving()
    {
        Vector2 maskForce = Vector2.one;
        //return maskForce;
        if (moveDirection.x != 0 && moveDirection.y == 0) maskForce = new Vector2(0.5f, 1f);
        if (moveDirection.x == 0 && moveDirection.y != 0) maskForce = new Vector2(1f, 0.5f);
        return maskForce;
    }
}
