﻿using UnityEngine;
using System.Collections;

public class GishState : MonoBehaviour
{

    public delegate void CollisionStart(float power);

    public event CollisionStart collisionStart;

    public bool isMoving = false;
    public float CollisionPower;
    private Vector2 lastSpeed = Vector2.zero;
    private Vector2 sumSpeed = Vector2.zero;
    private Vector2 sumChange = Vector2.zero;
    private Vector2 currentSpeed = Vector2.zero;
    private Vector2 collisionSpeed = Vector2.zero;
    //private Vector2 CollisionChangeSpeed = Vector2.zero;
    private float CollisionChangeValue = 0;
    private int counter = 0;
    private int counterChanges = 0;
    private float mass = -1f;
    public float middleChange = 0;
    public Vector2 maxSpeed = Vector2.zero;
    public float maxSpeedValue = 0f;
    public float maxChange = 0f;
    public Rigidbody2D[] rigids = null;
    public CollisionDetector[] detectors = null;
    private int collisionCounter = 0;
    private GishControllerNew controller = null;

    private enum Etap
    {
        Ground,
        InAir,
        Collision
    }
    private Etap etap = Etap.Ground;
    private int frameInAir = 0;
    public int FrameCollisionCheck = 10;
    private int frameCollision = 0;
    public float CheckCollisonForTime = 0.3f;
    private float timeCollisionCheck = 0.3f;

    #region Property
    public Vector2 Velocity
    {
        get { return rigids.Length > 0 ? rigids[0].velocity : Vector2.zero; }
        set
        {
            if (Input.GetKey(KeyCode.RightControl))
                Debug.Log("Start check");
            foreach (Rigidbody2D rigid in rigids)
            {
                rigid.velocity = value;
            }
        }
    }

    //Центр гиша
    public Vector3 Center
    {
        get
        {
            Vector3 center;

            float sumWeight = 0f;
            Vector2 c = Vector3.zero;
            foreach (Rigidbody2D rigid in rigids)
            {
                c += rigid.worldCenterOfMass * rigid.mass;
                sumWeight += rigid.mass;
            }
            c = c / sumWeight;
            center = c;

            return center;
        }
    }

    // масса капли складывается из массы всех rigidbody
    public float Mass
    {
        get
        {
            if (mass < 0)
            {
                mass = 0f;
                foreach (Rigidbody2D rigid in rigids)
                {
                    mass += rigid.mass;
                }
            }
            return mass;
        }
    }

    public float CurrentSpeedMagnitude
    {
        get { return currentSpeed.magnitude; }
    }
    public float LastSpeedMagnitude
    {
        get { return lastSpeed.magnitude; }
    }

    public float MiddleSpeedChange
    {
        get
        {
            if (counterChanges <= 0) return 0;
            float newVal = sumChange.magnitude / counterChanges;
            middleChange = middleChange > newVal ? middleChange : newVal;
            return middleChange;
        }
    }

    public float MaxSpeedValue
    {
        get { return maxSpeedValue; }
    }

    public Vector2 MovingSpeed
    {
        get
        {
            Vector2 sum = Vector2.zero;
            foreach (CollisionDetector detector in detectors)
            {
                sum += detector.rigid.velocity;
            }
            return detectors.Length > 0 ? sum / detectors.Length : Vector2.zero;
        }
    }

    public bool inAir = false;
    #endregion
    // Update is called once per frame
    void Awake()
    {
        rigids = GetComponentsInChildren<Rigidbody2D>();
        detectors = GetComponentsInChildren<CollisionDetector>();

        Clear();
        lastSpeed = Vector2.zero;
        isMoving = false;
        etap = Etap.InAir;
        CollisionChangeValue = 0;
    }

    // Use this for initialization
    void FixedUpdate()
    {
        inAir = controller.isTouch(controller.MoveMask);
        switch (etap)
        {
            case Etap.Ground:
                if (inAir)
                {
                    timeCollisionCheck = 0f;
                    etap = Etap.InAir;
                }
                break;
            case Etap.InAir:
                if (!inAir)
                {
                    etap = Etap.Collision;
                    collisionSpeed = lastSpeed;
                    timeCollisionCheck = CheckCollisonForTime;
                }
                else
                {
                    lastSpeed = MovingSpeed;
                }
                break;
            case Etap.Collision:
                timeCollisionCheck -= Time.fixedDeltaTime;
                if (timeCollisionCheck <= 0)
                {
                    etap = Etap.Ground;
                    endCollision();
                    break;

                }
                else if (inAir)
                {
                    etap = Etap.InAir;
                    endCollision();
                    break;
                }
                recalcCollisionEnergy();
                break;
        }
        //if (CollisionPower > 0)
        /*if (sumSpeed.sqrMagnitude > 0)
        {
            isMoving = false;
            OnCollisionStart(maxChange);
            maxChange = 0;
        }
        else
        {
            isMoving = true;
            maxChange = 0;
        }
        lastSpeed = currentSpeed;*/
        Clear();
    }

    /*public void SetPower(float value)
    {
        if (CollisionPower < value) CollisionPower = value;
    }*/
    public void SetVelocity(Vector2 velocity)
    {
        Vector2 old = lastSpeed;

        counter++;
        sumSpeed += velocity;
        currentSpeed = sumSpeed / counter;
        if (old.sqrMagnitude > float.Epsilon)
        {
            Vector2 change = (old - velocity) * Time.fixedDeltaTime;
            if (Vector2.Dot(change, old) < 0)
            {
                sumChange += change;
                counterChanges++;
                float value = change.magnitude;
                if (maxSpeedValue < value)
                {
                    maxSpeed = velocity;
                    maxSpeedValue = value;
                }
                if (value > maxChange)
                {
                    maxChange = value;
                }
                //CollisionPower = value;
                //CollisionPower += value;
            }
        }
    }

    public void SetController(GishControllerNew controller)
    {
        this.controller = controller;
    }

    public void CollisionCount()
    {
        if (etap == Etap.InAir)
        {
            etap = Etap.Collision;
            frameInAir = 0;
            frameCollision = FrameCollisionCheck;
        }
        collisionCounter++;
    }

    public void OnCollisionStart(float power)
    {
        if (collisionStart != null) collisionStart(power);
    }

    public void Clear()
    {
        CollisionPower = 0f;
        sumSpeed = Vector2.zero;
        // maxSpeed = Vector2.zero;
        //maxSpeedValue = 0f;
        currentSpeed = Vector2.zero;
        counter = 0;
        counterChanges = 0;
        collisionCounter = 0;
    }

    private void endCollision()
    {
        timeCollisionCheck = 0f;
        etap = Etap.Ground;
        //float collisionSpeedValue = collisionSpeed.magnitude;
        float change = - CollisionChangeValue * collisionSpeed.magnitude;
        //Debug.Log(string.Format("collisionSpeed: {0}  CollisionChangeValue: {1}", collisionSpeed.magnitude, CollisionChangeValue));
        OnCollisionStart(change);
        //CollisionChangeSpeed = Vector2.zero;
        CollisionChangeValue = 0f;
    }

    private void recalcCollisionEnergy()
    {
        foreach (CollisionDetector detector in detectors)
        {
            Vector2 velocity = detector.rigid.velocity;
            float changeValue = Vector2.Dot(velocity, collisionSpeed)/collisionSpeed.magnitude;
            if (changeValue < CollisionChangeValue && CollisionChangeValue > -1)
            {
                changeValue = changeValue < -1 ? -1 : changeValue; 
                CollisionChangeValue = changeValue;
            }
        }
    }

}
