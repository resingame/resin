﻿using UnityEngine;
using System.Collections;
using UnityEngine.Assertions.Comparers;

[RequireComponent(typeof(Rigidbody2D), typeof(CircleCollider2D))]
public class GishCenter : MonoBehaviour {

    public Vector2 moveDirection = Vector2.zero;
    //ограничение скорости
    public float VelocityLimit = 3;
    // твердые тела
    public Rigidbody2D rigid = null;
    private CircleCollider2D collider = null;
    public LayerMask MoveMask = Physics2D.AllLayers;
    public LayerMask StickMask = Physics2D.AllLayers;
    public bool IsStick = false;
    //сила на перемещение по горизонтали
    public float forceMove = 3;
    //сила прилипания к поверхности
    public float forceStick = 7;
    public float distanceCheck = 0.1f;

    public bool HasContactWall
    {
        get { return collider.IsTouchingLayers(MoveMask); }  
    }

    private PhysicsMaterial2D material = null;
    public Vector2 stickForce;
    private  Vector2 moveForce;
    private float gravityScale;
    // Use this for initialization
    void Awake ()
    {
        rigid = GetComponent<Rigidbody2D>();
        collider = GetComponent<CircleCollider2D>();
        material = collider.sharedMaterial;
        gravityScale = rigid.gravityScale;
    }
	
	// Update is called once per frame
	void Update () {
	}

    void FixedUpdate()
    {
        bool hasContact = HasContactWall;
        // расчет прилипания если не нажата клавиша прыжок, иначе мы отцепляемся
        if (IsStick)
        {
            //определяем направление в котором мы должны прилипать
            Vector2 dirStick = CalcStickForce();
            //прикладываем усилие в отношении всей капли
            AddForce(dirStick*forceStick);
            //нейтрализуем гравитацию для объекта
            if (hasContact) rigid.AddForce(-Physics2D.gravity);
        }
        
        //расчитываем движение по клавишам
        Vector2 dirMove = CalcMoveForce();
        Vector2 speed = rigid.velocity;
        // горизонтальное смещение, если оно мало из за наклона поверхности то считаем что мы в полете
        float x =  hasContact ? moveDirection.x * forceMove : 0;
        // вертикальное смещение
        float y = IsStick && hasContact? moveDirection.y * forceMove: 0;
        Vector2 f = new Vector2(x, y);
        float m = dirMove.magnitude;
        if (m != 0) f = Vector2.Dot(f, dirMove)/m*dirMove;
        //Debug.DrawLine(transform.position, f.normalized, Color.yellow);
        // если двигаемся вверх надо прикладывать больше усилий, чтобы одолеть гравитацию.
        AddForce(f);
        RecalcVelocity(f.normalized);
    }

    void RecalcVelocity(Vector2 moveForce)
    {
        if (HasContactWall && (moveForce.x != 0 || moveForce.y != 0))
        {
            rigid.velocity = Vector2.ClampMagnitude(rigid.velocity, VelocityLimit);
        }
    }

    /// <summary>
    /// Приложить силу к капле
    /// </summary>
    /// <param name="forceValue"> значение силы </param>
    /// <param name="forceMode"> режим приложения силы</param>
    /// <param name="mask">Маска слоев при соприкосновении с которыми срабатывает сила. 
    public void AddForce(Vector2 forceValue, ForceMode2D forceMode = ForceMode2D.Force, int mask = -1)
    {
        if (mask != -1 && !collider.IsTouchingLayers(mask)) return;
        rigid.AddForce(forceValue * Time.fixedDeltaTime, forceMode);
    }

    //Рассчитать направление силы  для прилипания при соприкосновении с поверхностью
    public Vector2 CalcStickForce()
    {

        stickForce = Vector2.zero;
        RaycastHit2D[] hits = Physics2D.CircleCastAll(transform.position, transform.TransformVector(Vector3.up * collider.radius * (1 + distanceCheck)).y, Vector2.zero, 0, StickMask) ; 
        Vector2 maskForce = Vector2.one;
        if (moveDirection.x != 0 && moveDirection.y == 0) maskForce = new Vector2(0.5f, 1f);
        if (moveDirection.x == 0 && moveDirection.y != 0)maskForce = new Vector2(1f, 0.5f);

        for (int i = 0; i < hits.Length; i++)
        {

            RaycastHit2D hit = hits[i];
            if (hit.collider != null)
            {
                Vector2 n = hit.normal;
                n.Scale(maskForce);
                stickForce += -n;
            }
        }
        stickForce = stickForce.normalized;
        //if (stickForce.magnitude == 0) Debug.Log("Сила не притягивает.");
        Debug.DrawRay(transform.position, stickForce, Color.magenta);
        return stickForce; 

    }
    /// <summary>
    /// рассчитать силу перемещения в пространстве исходя из точек соприкосновения с поверхностью
    /// </summary>
    /// <returns></returns>
    public Vector2 CalcMoveForce()
    {
        moveForce = Vector2.zero;
        RaycastHit2D[] hits = Physics2D.CircleCastAll(transform.position, transform.TransformVector(Vector3.up * collider.radius).y * (1 + distanceCheck), Vector2.zero, 0, StickMask);
        int activeRigids = 0;
        Vector2 maskForce = Vector2.one;
        if (moveDirection.x != 0 && moveDirection.y == 0) maskForce = new Vector2(0.5f, 1f);
        if (moveDirection.x == 0 && moveDirection.y != 0) maskForce = new Vector2(1f, 0.5f);
        for (int i = 0; i < hits.Length; i++)
        {

            RaycastHit2D hit = hits[i];
            if (hit.collider != null)
            {
                Vector2 n = hit.normal;
                if (IsStick) n.Scale(maskForce);
                float vert = n.x;
                float horz = -n.y;
                

                /*if (Mathf.Abs(vert) < float.Epsilon || Mathf.Abs(horz) < float.Epsilon)
                {
                    vert = Mathf.Abs(vert);
                    horz = Mathf.Abs(horz);
                }
                else
                {
                    float vs = Mathf.Sign(vert);
                    float hs = Mathf.Sign(horz);
                    horz = Mathf.Abs(horz);
                    if (vs != hs)
                    {
                        vert = -Mathf.Abs(vert);
                    }
                    else
                    {
                        vert = Mathf.Abs(vert);
                    }
                }*/
                moveForce += Vector2.right*horz + Vector2.up*vert;
                Debug.DrawRay(transform.position, hit.normal, Color.cyan);

            }
        }
        moveForce = moveForce.normalized;
        Debug.DrawRay(transform.position, moveForce, Color.green);
        return moveForce;
    }


}
