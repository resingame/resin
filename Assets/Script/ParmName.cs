﻿using UnityEngine;
using System.Collections;

public class ParmName {

    public const string MATCH = "p_match";
    public const string TIME = "p_time";
    public const string DAMAGE = "p_damage";
    public const string ELEMENT = "p_element";
    public const string WARRIOR = "p_warrior";
}
