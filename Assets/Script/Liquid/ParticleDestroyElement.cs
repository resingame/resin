﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ParticleDestroyElement : LPDrawParticleSystem
{
    public float LifeTime = 10f;
    private bool isStart = false;
    // Use this for initialization
	void Awake ()
	{
	    isStart = true;
	}

    public override void UpdateParticles(LPParticle[] partdata)
    {
        /*if (GetComponent<ParticleEmitter>().particleCount < partdata.Length)
        {
            GetComponent<ParticleEmitter>().Emit(partdata.Length - GetComponent<ParticleEmitter>().particleCount);
            particles = GetComponent<ParticleEmitter>().particles;
        }
        List<int> idxs = new List<int>();
        for (int i = 0; i < particles.Length; i++)
        {
            if (i > partdata.Length - 1)
            {
                particles[i].energy = 0f;
            }
            else
            {
                particles[i].position = partdata[i].Position;
                if (partdata[i].LifeTime < LifeTime/2f && Random.value > 0.2f)
                {
                    partdata[i].LifeTime = 0f;
                }
            }
        }

        GetComponent<ParticleEmitter>().particles = particles;*/

        if (GetComponent<ParticleEmitter>().particleCount < partdata.Length)
        {
            GetComponent<ParticleEmitter>().Emit(partdata.Length - GetComponent<ParticleEmitter>().particleCount);
            particles = GetComponent<ParticleEmitter>().particles;
        }

        for (int i = 0; i < particles.Length; i++)
        {
            if (i > partdata.Length - 1)
            {
                particles[i].energy = 0f;
            }
            else
            {
                particles[i].position = partdata[i].Position;
                float val = partdata[i].LifeTime;
                Color c = partdata[i]._Color;
                c.a = val;
                particles[i].color = c;
            }
        }

        GetComponent<ParticleEmitter>().particles = particles;
    }
}



