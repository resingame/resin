﻿using UnityEngine;
using System.Collections;

public class LiquidManager : MonoBehaviour {

    private static LPManager lpman;

    public static LPManager LpManager
    {
        get
        {
            if (lpman == null) lpman = FindObjectOfType<LPManager>();
                return lpman;
        }
        set { lpman = value; }
    }
}
