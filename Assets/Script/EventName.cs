﻿using UnityEngine;
using System.Collections;

public class EventName
{
    public const string MAP_CREATE_START = "e_mapCreateStart";
    public const string MAP_CREATE_END = "e_mapCreateEnd";

    public const string NEXT_TEAM = "e_nextTeam";
    public const string NEXT_WARRIOR = "e_nextWarrior";
    public const string WARRIOR_READY = "e_warriorReady";
    public const string WARRIOR_GET_DAMAGE = "e_warriorGetDamage";
    public const string WARRIOR_GET_HEALS = "e_warriorGetHeals";
    public const string WARRIOR_DIE = "e_warriorDie";

    public const string TIME_START = "e_timeStart";
    public const string TIME_END = "e_timeEnd";

    public const string BONUS_GENERATE_START = "e_bonusCreateStart";
    public const string BONUS_GENERATE_END = "e_bonusCreateEnd";
    public const string BONUS_CREATE = "e_bonusCreate";
    public const string BONUS_READY = "e_bonusReady";

    public const string GAME_END = "e_gameEnd";

}
